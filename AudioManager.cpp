#include "AudioManager.hpp"

#include <algorithm>

#include "SoundFilePlus/Handler.hpp"
#include "SoundFilePlus/Source.hpp"
#include "Sofa/File.hpp"
#include "Sofa/Filter.hpp"
#include "Utils/ConvolutionFilter.hpp"

#include "Tests/BypassFilter.hpp"
#include "Tests/AmplitudeFilter.hpp"
#include "Tests/DelayFilter.hpp"
#include "Tests/LowpassFilter.hpp"
#include "Tests/PannerFilter.hpp"
#include "Tests/NoiseSource.hpp"
#include "Tests/SineSource.hpp"
#include "Tests/ClickSource.hpp"

AudioManager::AudioManager(unsigned sampleRate, size_t blockSize)
    : sampleRate(sampleRate),
      blockSize(blockSize),
      totalTimer("Total"),
      inputTimer("Input"),
      filterTimer("Filter"),
      outputTimer("Output"),
      atomicPosition(Sofa::Position(0, 0, 0)),
      sharedPosition(std::make_shared<Sofa::Position>(0, 0, 0)),
      limiter(sampleRate)
{
    static_assert(std::atomic<size_t>::is_always_lock_free);
}

AudioManager::~AudioManager()
{
    stop();
}

std::optional<std::string> AudioManager::start(PortAudioPlus::Device device)
{
    if(isRunning())
    {
        return "Already running.";
    }

    if(sources.size() == 0)
    {
        return "No sources set.";
    }

    if(filters.size() == 0)
    {
        return "No filters set.";
    }

    if(device.info.maxOutputChannels < static_cast<int>(nFilterOutputs))
    {
        return "Max output channels is less than 2.";
    }

    PortAudioPlus::Stream stream;
    stream.mode = PortAudioPlus::Mode::Play;
    stream.device = device;
    stream.sampleRate = sampleRate;
    stream.numChannels = device.info.maxOutputChannels;
    stream.framesPerBuffer = blockSize;

    nDirectOutputs = stream.numChannels - nFilterOutputs;

    inputSignals.resize(filters.size() + nDirectOutputs, Signal(blockSize, 0));
    filteredSignals.resize(filters.size(), std::vector<Signal>(2, Signal(blockSize, 0)));
    outputSignals.resize(stream.numChannels, Signal(blockSize, 0));

    limiter.setNumChannels(stream.numChannels);

    atomicPlaybackStates.clear();
    for(const auto& source : sources)
    {
        atomicPlaybackStates.push_back(AtomicBuffer<PlaybackState>(source->getState()));
    }

    if(!audioInterface.start(stream, *this))
    {
        return "Failed to start playback.";
    }

    return std::nullopt;
}

void AudioManager::stop()
{
    audioInterface.stop();
}

bool AudioManager::isRunning() const
{
    return audioInterface.isRunning();
}

void AudioManager::setPosition(const Sofa::Position& position)
{
    atomicPosition.store(position);
}

void AudioManager::loadTestSourcesAndFilters()
{
    if(!isRunning())
    {
        sources.push_back(AudioPlayer::makeUnique<SineSource>(blockSize, sampleRate, 200, 0.25));
        sources.push_back(AudioPlayer::makeUnique<NoiseSource>(blockSize, 0.1));
        sources.push_back(AudioPlayer::makeUnique<ClickSource>(blockSize, sampleRate, 1, 0.5));

        filters.push_back(std::make_unique<BypassFilter>(2));
        filters.push_back(ConvolutionFilter::makeUnique<AmplitudeFilter>(blockSize, 2, sampleRate, 1));
        filters.push_back(ConvolutionFilter::makeUnique<AmplitudeFilter>(blockSize, 2, sampleRate, 0.25));
        filters.push_back(ConvolutionFilter::makeUnique<DelayFilter>(blockSize, 2, sampleRate, 0.5, 0.5));
        filters.push_back(ConvolutionFilter::makeUnique<LowpassFilter>(blockSize, 2, sampleRate, 1000));
        filters.push_back(ConvolutionFilter::makeUnique<PannerFilter>(blockSize, sampleRate, sharedPosition));
    }
}

std::optional<std::string> AudioManager::addSource(const std::string& filename)
{
    if(isRunning())
    {
        return "Can't load sources while running.";
    }

    SoundFilePlus::Handler file(filename, SoundFilePlus::Mode::Read);

    if(!file.getSettings().isValid())
    {
        return "Audio file is not valid.";
    }

    if(file.getSettings().getSampleRate() != sampleRate)
    {
        return "Sample rate does not match.";
    }

    auto source = AudioPlayer::makeUnique<SoundFilePlus::Source>(blockSize, std::move(file));

    sources.push_back(std::move(source));

    return std::nullopt;
}

std::optional<std::string> AudioManager::addFilter(const std::string& filename)
{
    if(isRunning())
    {
        return "Can't load filters while running.";
    }

    Sofa::File file(filename);

    if(!file.isValid())
    {
        return "Sofa file is not valid.";
    }

    if(file.getSampleRate() != sampleRate)
    {
        return "Sample rate does not match.";
    }

    auto filter = ConvolutionFilter::makeUnique<Sofa::Filter>(file, blockSize, sharedPosition);

    filters.push_back(std::move(filter));

    return std::nullopt;
}

std::vector<std::string> AudioManager::getSourceNames() const
{
    std::vector<std::string> sourceNames;

    for(const auto& source : sources)
    {
        sourceNames.push_back(source->getName());
    }

    return sourceNames;
}

std::vector<std::string> AudioManager::getFilterNames() const
{
    std::vector<std::string> filterNames;

    for(const auto& filter : filters)
    {
        filterNames.push_back(filter->getName());
    }

    for(size_t iDirectOutput = 0; iDirectOutput < nDirectOutputs; ++iDirectOutput)
    {
        filterNames.push_back("Direct output " + std::to_string(iDirectOutput + 2));
    }

    return filterNames;
}

const LogSpectrum& AudioManager::getFilterSpectrum() const
{
    static LogSpectrum emptyFilterSpectrum;

    return emptyFilterSpectrum;
}

void AudioManager::setPlaybackCommand(const PlaybackCommand& command)
{
    atomicPlaybackCommands.push(command);
}

std::vector<PlaybackState> AudioManager::getPlaybackStates()
{
    std::vector<PlaybackState> states(atomicPlaybackStates.size(), PlaybackState(0));

    for(size_t iSource = 0; iSource < atomicPlaybackStates.size(); ++iSource)
    {
        atomicPlaybackStates[iSource].load(states[iSource]);
    }

    return states;
}

bool AudioManager::handleOutput()
{
    Duration::LocalTimerGuard timerGuard(totalTimer);

    // Read input data
    atomicPosition.load(*sharedPosition);

    PlaybackCommand command;
    while(atomicPlaybackCommands.pop(command))
    {
        if(command.iChannel < sources.size())
        {
            sources[command.iChannel]->setCommand(command);
        }
    }

    // Reset signals
    for(auto& signal : inputSignals)
    {
        std::fill(signal.begin(), signal.end(), 0);
    }

    for(auto& signals : filteredSignals)
    {
        for(auto& signal : signals)
        {
            std::fill(signal.begin(), signal.end(), 0);
        }
    }

    for(auto& signal : outputSignals)
    {
        std::fill(signal.begin(), signal.end(), 0);
    }

    // Generate input signals
    inputTimer.start();
    for(auto& source : sources)
    {
        source->read(inputSignals);
    }
    inputTimer.stop();

    // Generate filtered signals
    filterTimer.start();
    for(size_t iFilter = 0; iFilter < filters.size(); ++iFilter)
    {
        filters[iFilter]->run(inputSignals[iFilter], filteredSignals[iFilter]);
    }
    filterTimer.stop();

    // Generate output signals
    outputTimer.start();
    for(const auto& filteredSignal : filteredSignals)
    {
        assert(outputSignals.size() >= filteredSignal.size());

        auto out = outputSignals.begin();

        for(const auto& in : filteredSignal)
        {
            std::transform(in.begin(), in.end(), out->begin(), out->begin(), std::plus<double>());
            ++out;
        }
    }

    assert(inputSignals.size() == filters.size() + nDirectOutputs);
    assert(outputSignals.size() == nFilterOutputs + nDirectOutputs);
    for(size_t iDirectOutput = 0; iDirectOutput < nDirectOutputs; ++iDirectOutput)
    {
        auto& in = inputSignals[filters.size() + iDirectOutput];
        auto& out = outputSignals[nFilterOutputs + iDirectOutput];

        std::copy(in.begin(), in.end(), out.begin());
    }

    volumeControl.run(outputSignals);
    limiter.run(outputSignals);
    outputTimer.stop();

    // Write outputs
    buffer.output.set(outputSignals);
    for(size_t iSource = 0; iSource < sources.size(); ++iSource)
    {
        atomicPlaybackStates[iSource].store(sources[iSource]->getState());
    }

    return true;
}
