#pragma once

#include <vector>
#include <memory>
#include <mutex>
#include <optional>

#include "Sofa/Position.hpp"
#include "PortAudioPlus/Interface.hpp"
#include "Utils/Limiter.hpp"
#include "Utils/VolumeControl.hpp"
#include "Utils/AudioPlayer.hpp"
#include "Utils/AtomicBuffer.hpp"
#include "Utils/AtomicRingBuffer.hpp"
#include "Utils/PlaybackCommand.hpp"
#include "Utils/PlaybackState.hpp"
#include "Utils/Duration.hpp"

class FilterBase;
class LogSpectrum;

class AudioManager : public PortAudioPlus::CallbackHandler
{
    using Signal = std::vector<double>;

public:
    AudioManager(unsigned sampleRate, size_t blockSize);
    ~AudioManager();

    std::optional<std::string> start(PortAudioPlus::Device device);
    void stop();

    bool isRunning() const;
    void setPosition(const Sofa::Position& position);

    void loadTestSourcesAndFilters();
    std::optional<std::string> addSource(const std::string& filename);
    std::optional<std::string> addFilter(const std::string& filename);

    std::vector<std::string> getSourceNames() const;
    std::vector<std::string> getFilterNames() const;

    size_t getBlockSize() const { return blockSize; }
    unsigned getSampleRate() const { return sampleRate; }

    const LogSpectrum& getFilterSpectrum() const;

    void setPlaybackCommand(const PlaybackCommand& command);
    std::vector<PlaybackState> getPlaybackStates();

    void setOutputLevel(double level) { volumeControl.setAmplitude(level); }
    double getOutputLevel() const { return volumeControl.getAmplitude(); }
    double getOutputCompression() const { return limiter.getFactor(); }
    std::vector<double> getOutputPeak() const { return limiter.getPeak(); }

    void printTimer() const
    {
        totalTimer.print();
        inputTimer.print();
        filterTimer.print();
        outputTimer.print();
    }

private:
    unsigned sampleRate{44100};
    size_t blockSize{256};

    size_t nFilterOutputs{2};
    size_t nDirectOutputs{0};

    Duration::LocalTimer totalTimer;
    Duration::LocalTimer inputTimer;
    Duration::LocalTimer filterTimer;
    Duration::LocalTimer outputTimer;

    AtomicBuffer<Sofa::Position> atomicPosition;
    AtomicRingBuffer<PlaybackCommand> atomicPlaybackCommands;
    std::vector<AtomicBuffer<PlaybackState>> atomicPlaybackStates;

    std::shared_ptr<Sofa::Position> sharedPosition;

    std::vector<std::unique_ptr<AudioPlayer>> sources;
    std::vector<std::unique_ptr<FilterBase>> filters;

    std::vector<Signal> inputSignals;
    std::vector<std::vector<Signal>> filteredSignals;
    std::vector<Signal> outputSignals;
    VolumeControl volumeControl;
    Limiter limiter;

    // IMPORTANT: Construct last to be destructed first to stop
    //            audio thread before freeing resources (sources
    //            and filters)
    PortAudioPlus::Interface audioInterface;

    bool handleOutput() override;
};
