#include "UDPTracker.hpp"

struct RawData
{
    uint8_t reserved[2];
    float values[3];
} __attribute__((packed));

UDPTracker::UDPTracker(unsigned port, QObject* parent)
    : QUdpSocket(parent)
{
    bind(QHostAddress::LocalHost, port);

    connect(this, &QUdpSocket::readyRead, this, &UDPTracker::receive);
}

void UDPTracker::send(const Sofa::Position& position, unsigned port)
{
    RawData rawData;
    rawData.values[0] = position.getAzimuth() / 180.0 * M_PI;
    rawData.values[1] = position.getElevation() / 180.0 * M_PI;

    size_t dataSize = sizeof(RawData);
    char bytes[dataSize];
    memcpy(bytes, &rawData, dataSize);

    writeDatagram(bytes, dataSize, QHostAddress::LocalHost, port);
}

void UDPTracker::receive()
{
    while(hasPendingDatagrams())
    {
        size_t dataSize = sizeof(RawData);
        char bytes[dataSize];

        if(readDatagram(bytes, dataSize) == static_cast<int>(dataSize))
        {
            RawData rawData;
            memcpy(&rawData, bytes, dataSize);

            double azimuth = rawData.values[0] / M_PI * 180.0;
            double elevation = rawData.values[1] / M_PI * 180.0;

            emit positionReceived(Sofa::Position(azimuth, elevation, 0));
        }
    }
}
