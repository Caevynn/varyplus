#pragma once

#include <QUdpSocket>

#include "Sofa/Position.hpp"

class UDPTracker : public QUdpSocket
{
    Q_OBJECT

public:
    UDPTracker(unsigned port, QObject* parent = nullptr);

    void send(const Sofa::Position& position, unsigned port);

signals:
    void positionReceived(Sofa::Position position);

private slots:
    void receive();
};
