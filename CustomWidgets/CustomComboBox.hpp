#pragma once

#include <QComboBox>
#include <QKeyEvent>

class CustomComboBox : public QComboBox
{
public:
    CustomComboBox(QWidget* parent = nullptr)
        : QComboBox(parent)
    {
        setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    }

private:
    void keyPressEvent(QKeyEvent* event) override
    {
        if(event->modifiers().testFlag(Qt::ControlModifier))
        {
            event->ignore();
        }
        else
        {
            QComboBox::keyPressEvent(event);
        }
    }
};
