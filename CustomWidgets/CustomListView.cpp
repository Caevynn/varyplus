#include "CustomListView.hpp"

#include <QKeyEvent>

CustomListView::CustomListView(QWidget* parent)
    : QListWidget(parent)
{}

void CustomListView::keyPressEvent(QKeyEvent* event)
{
    if(event->key() == Qt::Key_Delete)
    {
        if(count() > 0)
        {
            int row = currentRow();
            delete takeItem(row);
            emit itemRemoved(row);
        }

        event->accept();
    }
    else
    {
        QListWidget::keyPressEvent(event);
    }
}
