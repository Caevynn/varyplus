#pragma once

#include <QListWidget>

class CustomListView : public QListWidget
{
    Q_OBJECT

public:
    CustomListView(QWidget* parent = nullptr);

signals:
    void itemRemoved(int index);

private:
    void keyPressEvent(QKeyEvent* event) override;
};
