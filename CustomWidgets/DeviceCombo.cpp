#include "DeviceCombo.hpp"

#include "PortAudioPlus/DeviceList.hpp"

DeviceCombo::DeviceCombo(QWidget* parent)
    : CustomComboBox(parent)
{}

void DeviceCombo::update()
{
    std::vector<PortAudioPlus::Device> newDevices = PortAudioPlus::DeviceList::getOutputDevices();

    if(devices.size() == newDevices.size())
    {
        size_t nDevices = devices.size();

        bool hasChanged = false;

        for(size_t iDevice = 0; iDevice < nDevices; ++iDevice)
        {
            if(newDevices[iDevice].name != devices[iDevice].name)
            {
                hasChanged = true;
                break;
            }
        }

        if(!hasChanged)
        {
            return;
        }
    }

    std::string selectedDeviceName;

    if(!this->devices.empty())
    {
        selectedDeviceName = getDevice().name;
    }
    else
    {
        selectedDeviceName = PortAudioPlus::DeviceList::getDefaultOutputDevice().name;
    }

    devices.clear();
    QComboBox::clear();

    for(const auto& device : newDevices)
    {
        devices.push_back(device);
        addItem(QString::fromStdString(device.name));
    }

    for(size_t i = 0; i < devices.size(); ++i)
    {
        if(devices.at(i).name == selectedDeviceName)
        {
            setCurrentIndex(i);
            break;
        }
    }
}
