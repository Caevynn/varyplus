#pragma once

#include "CustomComboBox.hpp"

#include "PortAudioPlus/Interface.hpp"

class DeviceCombo : public CustomComboBox
{
    Q_OBJECT

public:
    DeviceCombo(QWidget* parent = nullptr);

    const PortAudioPlus::Device& getDevice() const
    {
        return devices.at(currentIndex());
    }

public slots:
    void update();

private:
    std::vector<PortAudioPlus::Device> devices;
};
