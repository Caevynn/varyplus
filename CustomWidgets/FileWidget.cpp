#include "FileWidget.hpp"

#include <QGridLayout>
#include <QTabWidget>
#include <QLabel>
#include <QListWidget>
#include <QKeyEvent>

#include "CustomListView.hpp"
#include "Utils/FileName.hpp"

FileWidget::FileWidget(QWidget* parent)
    : QGroupBox(parent)
{
    sourceView = new CustomListView;
    connect(sourceView, &CustomListView::itemRemoved, this, &FileWidget::removeSourceFile);

    filterView = new CustomListView;
    connect(filterView, &CustomListView::itemRemoved, this, &FileWidget::removeFilterFile);

    tabs = new QTabWidget;
    tabs->addTab(sourceView, "Sources (0)");
    tabs->addTab(filterView, "Filters (0)");
    tabs->setDocumentMode(true);
    tabs->tabBar()->setExpanding(true);

    QGridLayout* layout = new QGridLayout;
    layout->addWidget(tabs);

    setLayout(layout);
    setTitle("Loaded files");
}

void FileWidget::setEnabled(bool enabled)
{
    sourceView->setEnabled(enabled);
    filterView->setEnabled(enabled);
}

void FileWidget::addSourceFile(const std::string& fileName)
{
    for(const auto& sourceFile : sourceFiles)
    {
        if(sourceFile == fileName)
        {
            return;
        }
    }

    sourceFiles.push_back(fileName);

    sourceView->addItem(QString::fromStdString(FileName::removePath(fileName)));

    tabs->setTabText(0, "Sources (" + QString::number(sourceView->count()) + ")");
}

void FileWidget::addFilterFile(const std::string& fileName)
{
    for(const auto& filterFile : filterFiles)
    {
        if(filterFile == fileName)
        {
            return;
        }
    }

    filterFiles.push_back(fileName);

    filterView->addItem(QString::fromStdString(FileName::removePath(fileName)));

    tabs->setTabText(1, "Filters (" + QString::number(filterView->count()) + ")");
}

void FileWidget::removeSourceFile(int index)
{
    sourceFiles.erase(sourceFiles.begin() + index);

    tabs->setTabText(0, "Sources (" + QString::number(sourceView->count()) + ")");
}

void FileWidget::removeFilterFile(int index)
{
    filterFiles.erase(filterFiles.begin() + index);

    tabs->setTabText(1, "Filters (" + QString::number(filterView->count()) + ")");
}
