#pragma once

#include <QGroupBox>

class CustomListView;
class QTabWidget;

class FileWidget : public QGroupBox
{
    Q_OBJECT

public:
    FileWidget(QWidget* parent = nullptr);

    void setEnabled(bool enabled);
    void addSourceFile(const std::string& fileName);
    void addFilterFile(const std::string& fileName);

    const std::vector<std::string>& getSourceFiles() const { return sourceFiles; }
    const std::vector<std::string>& getFilterFiles() const { return filterFiles; }

private slots:
    void removeSourceFile(int index);
    void removeFilterFile(int index);

private:
    QTabWidget* tabs;
    CustomListView* sourceView;
    CustomListView* filterView;

    std::vector<std::string> sourceFiles;
    std::vector<std::string> filterFiles;
};
