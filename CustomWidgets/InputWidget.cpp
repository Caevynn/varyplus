#include "InputWidget.hpp"

#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

#include "NameCombo.hpp"
#include "PlaybackSlider.hpp"
#include "LevelSlider.hpp"

InputWidget::InputWidget(QWidget* parent)
    : QGroupBox(parent)
{
    playPauseButton = new QPushButton("Play");
    connect(playPauseButton, &QPushButton::clicked, this, &InputWidget::playClicked);

    sourceCombo = new NameCombo;

    filterCombo = new NameCombo;
    connect(filterCombo, &NameCombo::indexChanged, this, &InputWidget::filterChanged);

    playbackSlider = new PlaybackSlider;
    connect(playbackSlider, &PlaybackSlider::positionChanged, this, &InputWidget::positionChanged);

    levelSlider = new LevelSlider;
    connect(levelSlider, &LevelSlider::valueChanged, this, &InputWidget::levelChanged);

    QGridLayout* layout = new QGridLayout;
    layout->addWidget(playPauseButton, 0, 0, 1, 4);
    layout->addWidget(new QLabel("From"), 1, 0, 1, 1);
    layout->addWidget(sourceCombo, 1, 1, 1, 1);
    layout->addWidget(new QLabel("to"), 1, 2, 1, 1);
    layout->addWidget(filterCombo, 1, 3, 1, 1);
    layout->addWidget(playbackSlider, 2, 0, 1, 4);
    layout->addWidget(levelSlider, 3, 0, 1, 4);
    layout->addItem(new QSpacerItem(0, 0, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding), layout->rowCount(), 0, 1, layout->columnCount());

    setLayout(layout);
    setTitle("Input control");
}

void InputWidget::setEnabled(bool enabled)
{
    playPauseButton->setEnabled(enabled);
    sourceCombo->setEnabled(enabled);
    filterCombo->setEnabled(enabled);

    if(!enabled)
    {
        sourceCombo->update(std::vector<std::string>());
        filterCombo->update(std::vector<std::string>());
    }
}

void InputWidget::updateSources(const std::vector<std::string>& sourceNames)
{
    sourceCombo->update(sourceNames);
}

void InputWidget::updateFilters(const std::vector<std::string>& filterNames)
{
    filterCombo->update(filterNames);
}

void InputWidget::updateStates(const std::vector<PlaybackState>& states, unsigned sampleRate)
{
    if(sourceCombo->getIndex() < states.size())
    {
        auto state = states[sourceCombo->getIndex()];

        playPauseButton->setText(state.isRunning() ? "Pause" : "Play");
        if(filterCombo->getIndex() != state.getOutput())
        {
            filterCombo->setIndex(state.getOutput());
        }
        playbackSlider->setPosition(state.getPosition(), sampleRate);
        if(std::abs(levelSlider->getValue() - state.getVolume()) > 1e-7)
        {
            levelSlider->setValue(state.getVolume());
        }
    }
}

void InputWidget::playClicked()
{
    emit command(PlaybackCommand(sourceCombo->getIndex(), PlayCommand(playPauseButton->text() == "Play")));
}

void InputWidget::filterChanged(int index)
{
    emit command(PlaybackCommand(sourceCombo->getIndex(), OutputCommand(index)));
}

void InputWidget::positionChanged(size_t position)
{
    emit command(PlaybackCommand(sourceCombo->getIndex(), PositionCommand(position)));
}

void InputWidget::levelChanged(double level)
{
    emit command(PlaybackCommand(sourceCombo->getIndex(), VolumeCommand(level)));
}
