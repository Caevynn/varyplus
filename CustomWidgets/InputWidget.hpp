#pragma once

#include <QGroupBox>

#include "Utils/PlaybackState.hpp"

class QPushButton;
class NameCombo;
class PlaybackSlider;
class LevelSlider;

class InputWidget : public QGroupBox
{
    Q_OBJECT

public:
    InputWidget(QWidget* parent = nullptr);

    void setEnabled(bool enabled);
    void updateSources(const std::vector<std::string>& sourceNames);
    void updateFilters(const std::vector<std::string>& filterNames);
    void updateStates(const std::vector<PlaybackState>& states, unsigned sampleRate);

signals:
    void command(PlaybackCommand command);

private slots:
    void playClicked();
    void filterChanged(int index);
    void positionChanged(size_t position);
    void levelChanged(double level);

private:
    QPushButton* playPauseButton;
    NameCombo* sourceCombo;
    NameCombo* filterCombo;
    PlaybackSlider* playbackSlider;
    LevelSlider* levelSlider;
};
