#include "InterfaceWidget.hpp"

#include <QGridLayout>
#include <QPushButton>
#include <QLabel>

#include "DeviceCombo.hpp"
#include "NumberCombo.hpp"

InterfaceWidget::InterfaceWidget(QWidget* parent)
    : QGroupBox(parent)
{
    startStopButton = new QPushButton("Start");
    connect(startStopButton, &QPushButton::clicked, this, &InterfaceWidget::startClicked);

    deviceCombo = new DeviceCombo;
    deviceCombo->update();

    blockSizeCombo = new NumberCombo;
    blockSizeCombo->setValues({64, 128, 256, 512, 1024, 2048}, 2);

    sampleRateCombo = new NumberCombo;
    sampleRateCombo->setValues({44100, 48000, 88200, 96000});

    refreshButton = new QPushButton("Refresh");
    connect(refreshButton, &QPushButton::clicked, deviceCombo, &DeviceCombo::update);

    QGridLayout* layout = new QGridLayout;
    layout->addWidget(startStopButton, 0, 0, 1, 2);
    layout->addWidget(new QLabel("Device:"), 1, 0, 1, 1);
    layout->addWidget(deviceCombo, 1, 1, 1, 1);
    layout->addWidget(new QLabel("Block size:"), 2, 0, 1, 1);
    layout->addWidget(blockSizeCombo, 2, 1, 1, 1);
    layout->addWidget(new QLabel("Sample rate:"), 3, 0, 1, 1);
    layout->addWidget(sampleRateCombo, 3, 1, 1, 1);
    layout->addWidget(refreshButton, 4, 0, 1, 2);
    layout->addItem(new QSpacerItem(0, 0, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding), layout->rowCount(), 0, 1, layout->columnCount());

    setTitle("Audio interface");
    setLayout(layout);
}

void InterfaceWidget::setEnabled(bool enabled)
{
    startStopButton->setText(enabled ? "Start" : "Stop");
    deviceCombo->setEnabled(enabled);
    blockSizeCombo->setEnabled(enabled);
    sampleRateCombo->setEnabled(enabled);
    refreshButton->setEnabled(enabled);
}

PortAudioPlus::Device InterfaceWidget::getDevice() const
{
    return deviceCombo->getDevice();
}

size_t InterfaceWidget::getBlockSize() const
{
    return blockSizeCombo->getValue();
}

unsigned InterfaceWidget::getSampleRate() const
{
    return sampleRateCombo->getValue();
}

