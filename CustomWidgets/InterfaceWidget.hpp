#pragma once

#include <QGroupBox>

#include "PortAudioPlus/Interface.hpp"

class QPushButton;
class DeviceCombo;
class NumberCombo;

class InterfaceWidget : public QGroupBox
{
    Q_OBJECT

public:
    InterfaceWidget(QWidget* parent = nullptr);

    void setEnabled(bool enabled);

    PortAudioPlus::Device getDevice() const;
    size_t getBlockSize() const;
    unsigned getSampleRate() const;

signals:
    void startClicked();

private:
    QPushButton* startStopButton;
    DeviceCombo* deviceCombo;
    NumberCombo* blockSizeCombo;
    NumberCombo* sampleRateCombo;
    QPushButton* refreshButton;
};
