#include "LevelMeter.hpp"

#include <QFrame>
#include <QRect>
#include <QPainter>
#include <QLabel>
#include <QHBoxLayout>
#include <QMouseEvent>

#include "Utils/LogTaper.hpp"

LevelMeter::LevelMeter(bool inverted, QWidget* parent)
    : QWidget(parent)
{
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    text = new QLabel;
    text->setAlignment(Qt::AlignCenter);
    QFontMetrics metrics = text->fontMetrics();
    int textWidth = metrics.boundingRect("-00.00").width();
    text->setFixedWidth(2 * textWidth);
    layout->addWidget(text);

    display = new LevelMeterDisplay(inverted);
    layout->addWidget(display);

    reset();
}

void LevelMeter::update(std::vector<double> values)
{
    display->update(values);

    updateText();
}

void LevelMeter::setEnabled(bool enabled)
{
    if(!enabled)
    {
        display->update(std::vector<double>());
    }
}

void LevelMeter::reset()
{
    display->reset();

    updateText();
}

void LevelMeter::updateText()
{
    double maxValue = display->getMaxValue();

    text->setText(QString::number(LogTaper::toDB(maxValue), 'f', 2));
}

void LevelMeter::mousePressEvent(QMouseEvent* event)
{
    reset();

    event->accept();
}

LevelMeterDisplay::LevelMeterDisplay(bool inverted, QWidget* parent)
    : QFrame(parent),
      inverted(inverted)
{
    setFrameStyle(QFrame::Panel | QFrame::Sunken);
}

void LevelMeterDisplay::reset()
{
    if(!inverted)
    {
        maxValues.clear();
    }

    QFrame::update();
}

void LevelMeterDisplay::update(std::vector<double> values)
{
    this->values = values;

    if(maxValues.size() != values.size())
    {
        maxValues.resize(values.size(), inverted ? 1.0 : 0.0);
    }

    if(inverted)
    {
        for(auto& value : values)
        {
            if(value > LogTaper::fromDB(-0.01))
            {
                value = 1.0;
            }
        }

        maxValues = values;
    }
    else
    {
        for(size_t i = 0; i < values.size(); ++i)
        {
            maxValues[i] = std::max(values[i], maxValues[i]);

            if(maxValues[i] < 1e-3)
            {
                maxValues[i] = 0;
            }
        }
    }

    QFrame::update();
}

double LevelMeterDisplay::getMaxValue() const
{
    double maxValue = inverted ? 1.0 : 0.0;

    for(const auto& value : maxValues)
    {
        if(inverted)
        {
            maxValue = std::min(value, maxValue);
        }
        else
        {
            maxValue = std::max(value, maxValue);
        }
    }

    return maxValue;
}

void LevelMeterDisplay::paintEvent(QPaintEvent* event)
{
    QFrame::paintEvent(event);

    QRect area;
    area.setTop(rect().top() + frameWidth());
    area.setBottom(rect().bottom() - frameWidth());
    area.setLeft(rect().left() + frameWidth());
    area.setRight(rect().right() - frameWidth());

    QPainter painter(this);
    painter.fillRect(area, Qt::black);

    int barHeight = area.height() / std::max(values.size(), size_t(1));

    QRect bar = area;
    bar.setTop(area.top() + 1);
    bar.setBottom(area.top() + barHeight - 1);

    if(!inverted)
    {
        double zeroPosition = LogTaper::toLog(1, 2);

        for(const auto& value : values)
        {
            double valuePosition = std::min(LogTaper::toLog(value, 2), 1.0);

            if(valuePosition > 1e-3)
            {
                bar.setRight(area.left() + valuePosition * area.width());
                painter.fillRect(bar, Qt::green);

                if(valuePosition > zeroPosition)
                {
                    bar.setLeft(area.left() + zeroPosition * area.width());
                    painter.fillRect(bar, Qt::red);
                    bar.setLeft(area.left());
                }
            }

            bar.setBottom(bar.bottom() + barHeight);
            bar.setTop(bar.top() + barHeight);
        }

        int maxLineTop = area.top() + 1;
        int maxLineBottom = area.top() + barHeight - 1;

        for(const auto& maxValue : maxValues)
        {
            if(maxValue > 0)
            {
                double maxPosition = std::min(LogTaper::toLog(maxValue, 2), 1.0);

                int maxLinePosition = area.left() + maxPosition * area.width();
                painter.setPen(Qt::red);
                painter.drawLine(maxLinePosition, maxLineTop, maxLinePosition, maxLineBottom);
            }

            maxLineTop += barHeight;
            maxLineBottom += barHeight;
        }

        int zeroLinePosition = area.left() + zeroPosition * area.width();
        painter.setPen(Qt::gray);
        painter.drawLine(zeroLinePosition, area.top(), zeroLinePosition, area.bottom());

    }
    else
    {
        for(const auto& value : values)
        {
            if(value <= LogTaper::fromDB(-0.01))
            {
                bar.setLeft(area.left() + LogTaper::toLog(value) * area.width());
                painter.fillRect(bar, Qt::red);

                bar.moveBottom(barHeight + 1);
                bar.moveTop(barHeight + 1);
            }
        }
    }
}
