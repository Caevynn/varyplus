#pragma once

#include <QFrame>

class QLabel;
class LevelMeterDisplay;

class LevelMeter : public QWidget
{
public:
    LevelMeter(bool inverted, QWidget* parent = nullptr);

    void setEnabled(bool enabled);
    void update(std::vector<double> values);

private:
    LevelMeterDisplay* display;
    QLabel* text;

    void reset();
    void updateText();
    void mousePressEvent(QMouseEvent* event) override;
};

class LevelMeterDisplay : public QFrame
{
public:
    LevelMeterDisplay(bool inverted, QWidget* parent = nullptr);

    void reset();
    void update(std::vector<double> values);

    double getMaxValue() const;

private:
    bool inverted;

    std::vector<double> values;
    std::vector<double> maxValues;

    void paintEvent(QPaintEvent* event) override;
};
