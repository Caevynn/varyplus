#include "LevelSlider.hpp"

#include <QHBoxLayout>
#include <QSlider>
#include <QLineEdit>
#include <QMouseEvent>
#include <QStyleOptionSlider>
#include <QPainter>

#include "Utils/LogTaper.hpp"

LevelSlider::LevelSlider(QWidget* parent)
    : QWidget(parent)
{
    text = new QLineEdit;
    text->setAlignment(Qt::AlignCenter);
    connect(text, &QLineEdit::editingFinished, this, &LevelSlider::textEdited);

    QFontMetrics metrics = text->fontMetrics();
    int textWidth = metrics.boundingRect("-00.00").width();
    text->setFixedWidth(2 * textWidth);

    slider = new LevelSliderSlider(Qt::Orientation::Horizontal);
    connect(slider, &LevelSliderSlider::valueChanged, this, &LevelSlider::sliderMoved);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(text);
    layout->addWidget(slider);

    updateText();
}

void LevelSlider::setValue(double value)
{
    slider->setValue(value);
    updateText();
}

void LevelSlider::setEnabled(bool enabled)
{
    text->setEnabled(enabled);
    slider->setEnabled(enabled);
}

double LevelSlider::getValue() const
{
    return slider->getValue();
}

void LevelSlider::textEdited()
{
    bool ok;
    double dBValue = text->text().toDouble(&ok);

    if(ok)
    {
        double value = LogTaper::fromDB(dBValue);
        slider->setValue(value);
        emit valueChanged(value);
    }

    updateText();
}

void LevelSlider::sliderMoved(double value)
{
    updateText();
    emit valueChanged(value);
}

void LevelSlider::updateText()
{
    text->setText(QString::number(LogTaper::toDB(slider->getValue()), 'f', 2));
    text->clearFocus();
}

LevelSliderSlider::LevelSliderSlider(Qt::Orientation orientation, QWidget* parent)
    : QSlider(orientation, parent),
      maxValue(LogTaper::fromDB(6))
{
    setRange(0, 1000);
    setValue(1);

    connect(this, &QSlider::valueChanged, this, &LevelSliderSlider::positionChanged);
}

void LevelSliderSlider::setValue(double value)
{
    currentValue = std::min(value, maxValue);

    bool oldState = blockSignals(true);
    QSlider::setValue(LogTaper::toLog(currentValue, maxValue) * maximum() + 0.5);
    blockSignals(oldState);
}

double LevelSliderSlider::getValue() const
{
    return currentValue;
}

void LevelSliderSlider::positionChanged(int position)
{
    double currentPosition = static_cast<double>(position) / maximum();
    setValue(LogTaper::fromLog(currentPosition, maxValue));
    emit valueChanged(currentValue);
}

QRect LevelSliderSlider::getHandle() const
{
    QStyleOptionSlider opt;
    initStyleOption(&opt);
    return style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);
}

void LevelSliderSlider::mousePressEvent(QMouseEvent* event)
{
    QRect handle = getHandle();

    switch(event->button())
    {
    case Qt::LeftButton:
        if(!handle.contains(event->pos()))
        {
            int handleHalfWidth = handle.width() / 2;
            int horizontalPosition = event->pos().x() - handleHalfWidth;
            int horizontalArea = width() - 2 * handleHalfWidth;

            double newPosition = static_cast<double>(horizontalPosition) / horizontalArea;
            setValue(LogTaper::fromLog(newPosition, maxValue));
            emit valueChanged(currentValue);
        }
        break;

    case Qt::RightButton:
        setValue(1);
        emit valueChanged(currentValue);
        break;

    default:
        break;
    }

    QSlider::mousePressEvent(event);
}

void LevelSliderSlider::paintEvent(QPaintEvent* event)
{
    QRect handle = getHandle();

    int handleHalfWidth = handle.width() / 2;
    int horizontalArea = width() - 2 * handleHalfWidth;
    int markPosition = handleHalfWidth + LogTaper::toLog(1, maxValue) * horizontalArea;

    QPainter painter(this);
    painter.setPen(Qt::gray);
    painter.drawLine(markPosition, rect().top(), markPosition, rect().bottom() - 2);

    QSlider::paintEvent(event);
}
