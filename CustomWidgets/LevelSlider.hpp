#pragma once

#include <QSlider>

class LevelSliderSlider;
class QLineEdit;

class LevelSlider : public QWidget
{
    Q_OBJECT

public:
    LevelSlider(QWidget* parent = nullptr);

    void setEnabled(bool enabled);
    void setValue(double value);

    double getValue() const;

signals:
    void valueChanged(double value);

private slots:
    void textEdited();
    void sliderMoved(double value);

private:
    LevelSliderSlider* slider;
    QLineEdit* text;

    void updatePosition();
    void updateText();
};

class LevelSliderSlider : public QSlider
{
    Q_OBJECT

public:
    LevelSliderSlider(Qt::Orientation orientation, QWidget* parent = nullptr);

    void setValue(double value);
    double getValue() const;

signals:
    void valueChanged(double value);

private slots:
    void positionChanged(int position);

private:
    double currentValue;
    double maxValue;

    QRect getHandle() const;

    void mousePressEvent(QMouseEvent* event) override;
    void paintEvent(QPaintEvent* event) override;
};
