#include "LinePlot.hpp"

#include <QStackedWidget>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>

#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QLogValueAxis>

#include <QGraphicsLayout>

#include "Utils/LogSpectrum.hpp"

LinePlot::LinePlot(QWidget* parent)
    : QWidget(parent)
{
    display = new LinePlotDisplay;
    display->setRange(min, max);

    QLabel* label = new QLabel("No data available.");
    label->setAlignment(Qt::AlignCenter);
    label->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QPalette palette = label->palette();
    palette.setColor(label->backgroundRole(), Qt::white);
    label->setPalette(palette);
    label->setAutoFillBackground(true);

    stack = new QStackedWidget;
    stack->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    stack->addWidget(display);
    stack->addWidget(label);

    minEdit = new QLineEdit(QString::number(min, 'f', 2));
    minEdit->setAlignment(Qt::AlignCenter);

    maxEdit = new QLineEdit(QString::number(max, 'f', 2));
    maxEdit->setAlignment(Qt::AlignCenter);

    QGridLayout* layout = new QGridLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(stack, 0, 0, 1, 4);
    layout->addWidget(new QLabel("Min:"), 1, 0, 1, 1);
    layout->addWidget(minEdit, 1, 1, 1, 1);
    layout->addWidget(new QLabel("Max:"), 1, 2, 1, 1);
    layout->addWidget(maxEdit, 1, 3, 1, 1);

    connect(minEdit, &QLineEdit::editingFinished, this, &LinePlot::rangeChanged);
    connect(maxEdit, &QLineEdit::editingFinished, this, &LinePlot::rangeChanged);

    setEnabled(false);
}

void LinePlot::setEnabled(bool enabled)
{
    stack->setCurrentIndex(enabled ? 0 : 1);
    minEdit->setEnabled(enabled);
    maxEdit->setEnabled(enabled);
}

void LinePlot::plot(const LogSpectrum& data)
{
    if(!data.empty())
    {
        display->plot(data);
    }

    setEnabled(!data.empty());
}

void LinePlot::rangeChanged()
{
    bool minOk;
    double minValue = minEdit->text().toDouble(&minOk);

    bool maxOk;
    double maxValue = maxEdit->text().toDouble(&maxOk);

    if(minOk && maxOk && minValue < maxValue)
    {
        min = minValue;
        max = maxValue;
        display->setRange(min, max);
    }

    minEdit->setText(QString::number(min, 'f', 2));
    maxEdit->setText(QString::number(max, 'f', 2));
}

LinePlotDisplay::LinePlotDisplay(QWidget* parent)
    : QtCharts::QChartView(parent)
{
    axisX = new QtCharts::QLogValueAxis;
    axisX->setTitleText("Frequency [kHz]");
    axisX->setBase(10);

    axisY = new QtCharts::QValueAxis;
    axisY->setTitleText("Amplitude [dB]");

    chart = new QtCharts::QChart;
    chart->legend()->hide();
    chart->addAxis(axisX, Qt::AlignBottom);
    chart->addAxis(axisY, Qt::AlignLeft);
    chart->layout()->setContentsMargins(0, 0, 0, 0);
    chart->setBackgroundRoundness(0);

    setChart(chart);
    setRenderHint(QPainter::Antialiasing);
}

void LinePlotDisplay::setRange(double min, double max)
{
    axisY->setRange(min, max);
}

void LinePlotDisplay::plot(const LogSpectrum& data)
{
    chart->removeAllSeries();

    if(data.empty())
    {
        return;
    }

    axisX->setRange(data.get()[0].front().frequency / 1000, data.get()[0].back().frequency / 1000);

    for(const auto& channel : data.get())
    {
        QtCharts::QLineSeries* series = new QtCharts::QLineSeries;

        for(const auto& point : channel)
        {
            series->append(QPointF(point.frequency / 1000, point.amplitude));
        }

        chart->addSeries(series);

        series->attachAxis(axisX);
        series->attachAxis(axisY);
    }
}
