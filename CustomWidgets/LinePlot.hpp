#pragma once

#include <QtCharts/QChartView>

class QLineEdit;
class QStackedWidget;
class LinePlotDisplay;
class LogSpectrum;

class LinePlot : public QWidget
{
public:
    LinePlot(QWidget* parent = nullptr);

    void setEnabled(bool enabled);
    void plot(const LogSpectrum& data);

private slots:
    void rangeChanged();

private:
    double min{-30};
    double max{30};

    QStackedWidget* stack;
    LinePlotDisplay* display;
    QLineEdit* minEdit;
    QLineEdit* maxEdit;
};

namespace QtCharts
{
class QLineSeries;
class QValueAxis;
class QLogValueAxis;
}

class LinePlotDisplay : public QtCharts::QChartView
{
public:
    LinePlotDisplay(QWidget* parent = nullptr);

    void plot(const LogSpectrum& data);

    void setRange(double min, double max);

private:
    QtCharts::QLogValueAxis* axisX;
    QtCharts::QValueAxis* axisY;
    QtCharts::QChart* chart;
};
