#include "NameCombo.hpp"

NameCombo::NameCombo(QWidget* parent)
    : CustomComboBox(parent)
{
    connect(this, qOverload<int>(&QComboBox::currentIndexChanged), this, &NameCombo::indexChanged);
}

void NameCombo::update(const std::vector<std::string>& newNames)
{
    if(names.size() - 1 == newNames.size())
    {
        size_t nNames = names.size() - 1;

        bool hasChanged = false;

        for(size_t iName = 0; iName < nNames; ++iName)
        {
            if(names[iName] != newNames[iName])
            {
                hasChanged = true;
                break;
            }
        }

        if(!hasChanged)
        {
            return;
        }
    }

    std::optional<std::string> selectedName;

    if(!names.empty())
    {
        selectedName = names.at(currentIndex());
    }

    names = newNames;

    QComboBox::clear();

    for(const auto& name : names)
    {
        addItem(QString::fromStdString(name));
    }

    if(selectedName)
    {
        for(size_t i = 0; i < names.size(); ++i)
        {
            if(names.at(i) == *selectedName)
            {
                setCurrentIndex(i);
                break;
            }
        }
    }
}

void NameCombo::setIndex(size_t index)
{
    if(index < names.size())
    {
        bool oldState = blockSignals(true);
        setCurrentIndex(index);
        blockSignals(oldState);
    }
}

size_t NameCombo::getIndex() const
{
    return currentIndex();
}
