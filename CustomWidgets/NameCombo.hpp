#pragma once

#include "CustomComboBox.hpp"

class NameCombo : public CustomComboBox
{
    Q_OBJECT

public:
    NameCombo(QWidget* parent = nullptr);

    void update(const std::vector<std::string>& newNames);

    void setIndex(size_t index);
    size_t getIndex() const;

signals:
    void indexChanged(int index);

private:
    std::vector<std::string> names;
};
