#pragma once

#include "CustomComboBox.hpp"

class NumberCombo : public CustomComboBox
{
public:
    NumberCombo(QWidget* parent = nullptr)
        : CustomComboBox(parent)
    {}

    void setValues(const std::vector<unsigned>& values, int defaultIndex = 0)
    {
        this->values = values;

        QComboBox::clear();

        for(const auto& value : values)
        {
            addItem(QString::number(value));
        }

        setCurrentIndex(defaultIndex);
    }

    unsigned getValue() const
    {
        return values.at(currentIndex());
    }

private:
    std::vector<unsigned> values;
};
