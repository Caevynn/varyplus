#include "OutputWidget.hpp"

#include <QVBoxLayout>

#include "LevelMeter.hpp"
#include "LevelSlider.hpp"

OutputWidget::OutputWidget(QWidget* parent)
    : QGroupBox(parent)
{
    levelSlider = new LevelSlider;
    connect(levelSlider, &LevelSlider::valueChanged, this, &OutputWidget::levelChanged);

    levelMeter = new LevelMeter(false);

    compressionMeter = new LevelMeter(true);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(levelSlider);
    layout->addWidget(levelMeter);
    layout->addWidget(compressionMeter);

    setLayout(layout);
    setTitle("Output control");
}

void OutputWidget::setEnabled(bool enabled)
{
    levelSlider->setEnabled(enabled);
    levelMeter->setEnabled(enabled);
    compressionMeter->setEnabled(enabled);
}

void OutputWidget::updatePeak(const std::vector<double>& peak)
{
    levelMeter->update(peak);
}

void OutputWidget::updateCompression(const std::vector<double>& compression)
{
    compressionMeter->update(compression);
}

double OutputWidget::getLevel() const
{
    return levelSlider->getValue();
}
