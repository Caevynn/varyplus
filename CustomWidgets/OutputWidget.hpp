#pragma once

#include <QGroupBox>

class LevelMeter;
class LevelSlider;

class OutputWidget : public QGroupBox
{
    Q_OBJECT

public:
    OutputWidget(QWidget* parent = nullptr);

    void setEnabled(bool enabled);
    void updatePeak(const std::vector<double>& peak);
    void updateCompression(const std::vector<double>& compression);

    double getLevel() const;

signals:
    void levelChanged(double volume);

private:
    LevelSlider* levelSlider;
    LevelMeter* levelMeter;
    LevelMeter* compressionMeter;
};
