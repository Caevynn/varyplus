#include "PlaybackSlider.hpp"

#include <QHBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QMouseEvent>
#include <QStyleOptionSlider>

PlaybackSlider::PlaybackSlider(QWidget* parent)
    : QWidget(parent)
{
    text = new QLabel("00:00 / 00:00");

    slider = new QSlider(Qt::Horizontal);
    slider->setRange(0, 100);
    slider->setTracking(false);
    connect(slider, &QSlider::sliderPressed, this, &PlaybackSlider::sliderPressed);
    connect(slider, &QSlider::sliderMoved, this, &PlaybackSlider::sliderMoved);
    connect(slider, &QSlider::sliderReleased, this, &PlaybackSlider::sliderReleased);

    auto* layout = new QHBoxLayout;
    layout->addWidget(slider);
    layout->addWidget(text);
    layout->setContentsMargins(0, 0, 0, 0);

    setLayout(layout);
}

void PlaybackSlider::setEnabled(bool enabled)
{
    if(!enabled)
    {
        setPosition(PlaybackPosition(), 0);
    }
}

void PlaybackSlider::setPosition(const PlaybackPosition& position, unsigned sampleRate)
{
    currentPosition = position;
    this->sampleRate = sampleRate;

    if(position.getLength() > 0)
    {
        if(!isDragging)
        {
            unsigned secondsPlayed = position.getPosition() / sampleRate;
            unsigned minutesPlayed = secondsPlayed / 60;
            secondsPlayed -= minutesPlayed * 60;

            unsigned secondsTotal= position.getLength() / sampleRate;
            unsigned minutesTotal = secondsTotal / 60;
            secondsTotal -= minutesTotal * 60;

            text->setText(QString::number(minutesPlayed).rightJustified(2, '0') + ":" +
                        QString::number(secondsPlayed).rightJustified(2, '0') + " / " +
                        QString::number(minutesTotal).rightJustified(2, '0') + ":" +
                        QString::number(secondsTotal).rightJustified(2, '0'));
            text->setEnabled(true);

            slider->setValue(position.getRelativePosition() * slider->maximum() + 0.5);
            slider->setEnabled(true);
        }
    }
    else
    {
        text->setText("00:00 / 00:00");
        text->setEnabled(false);
        slider->setValue(0);
        slider->setEnabled(false);
    }
}

void PlaybackSlider::sliderPressed()
{
    isDragging = true;
    draggingValue = slider->value();
}

void PlaybackSlider::sliderMoved(int value)
{
    draggingValue = value;

    double relativePosition = static_cast<double>(value) / slider->maximum();
    size_t position = relativePosition * currentPosition.getLength() + 0.5;

    unsigned secondsPlayed = position / sampleRate;
    unsigned minutesPlayed = secondsPlayed / 60;
    secondsPlayed -= minutesPlayed * 60;

    unsigned secondsTotal= currentPosition.getLength() / sampleRate;
    unsigned minutesTotal = secondsTotal / 60;
    secondsTotal -= minutesTotal * 60;

    text->setText(QString::number(minutesPlayed).rightJustified(2, '0') + ":" +
                QString::number(secondsPlayed).rightJustified(2, '0') + " / " +
                QString::number(minutesTotal).rightJustified(2, '0') + ":" +
                QString::number(secondsTotal).rightJustified(2, '0'));
}

void PlaybackSlider::sliderReleased()
{
    slider->setValue(draggingValue);

    double relativePosition = static_cast<double>(draggingValue) / slider->maximum();
    size_t position = relativePosition * currentPosition.getLength() + 0.5;

    emit positionChanged(position);

    isDragging = false;
}
