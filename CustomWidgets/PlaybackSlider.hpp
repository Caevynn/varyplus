#pragma once

#include <QWidget>

#include "Utils/PlaybackState.hpp"

class QSlider;
class QLabel;

class PlaybackSlider : public QWidget
{
    Q_OBJECT

public:
    PlaybackSlider(QWidget* parent = nullptr);

    void setEnabled(bool enabled);
    void setPosition(const PlaybackPosition& position, unsigned sampleRate);

signals:
    void positionChanged(size_t position);

private slots:
    void sliderPressed();
    void sliderMoved(int value);
    void sliderReleased();

private:
    bool isDragging{false};
    int draggingValue{0};
    unsigned sampleRate{44100};

    QLabel* text;
    QSlider* slider;

    PlaybackPosition currentPosition;
};
