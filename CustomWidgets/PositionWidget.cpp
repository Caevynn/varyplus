#include "PositionWidget.hpp"

#include <QVBoxLayout>

#include "Slider2D.hpp"
#include "CustomObjects/UDPTracker.hpp"

PositionWidget::PositionWidget(QWidget* parent)
    : QGroupBox(parent)
{
    slider = new Slider2D;

    udp = new UDPTracker(5555, this);
    connect(udp, &UDPTracker::positionReceived, this, &PositionWidget::positionReceived);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(slider);

    setLayout(layout);
    setTitle("Listener position");
}

Sofa::Position PositionWidget::getPosition() const
{
    return slider->getPosition();
}
void PositionWidget::positionReceived(const Sofa::Position& position)
{
    slider->setPosition(position);

    emit positionChanged(slider->getPosition());
}
