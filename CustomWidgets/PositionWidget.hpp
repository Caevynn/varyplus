#pragma once

#include <QGroupBox>

#include "Sofa/Position.hpp"

class Slider2D;
class UDPTracker;

class PositionWidget : public QGroupBox
{
    Q_OBJECT

public:
    PositionWidget(QWidget* parent = nullptr);

    Sofa::Position getPosition() const;

signals:
    void positionChanged(Sofa::Position position);

private slots:
    void positionReceived(const Sofa::Position& position);

private:
    Slider2D* slider;
    UDPTracker* udp;
};
