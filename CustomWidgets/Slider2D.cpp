#include "Slider2D.hpp"

#include <algorithm>

#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QFrame>
#include <QPainter>
#include <QMouseEvent>

Slider2D::Slider2D(bool interactionEnabled, QWidget* parent)
    : QWidget(parent),
      isInteractionEnabled(interactionEnabled)
{
    display = new Slider2DDisplay;
    display->setDraggingEnabled(isInteractionEnabled);
    connect(display, &Slider2DDisplay::positionChanged, this, &Slider2D::positionMoved);

    xEdit = new QLineEdit;
    xEdit->setAlignment(Qt::AlignCenter);
    xEdit->setEnabled(isInteractionEnabled);
    connect(xEdit, &QLineEdit::editingFinished, this, &Slider2D::positionEdited);

    yEdit = new QLineEdit;
    yEdit->setAlignment(Qt::AlignCenter);
    yEdit->setEnabled(isInteractionEnabled);
    connect(yEdit, &QLineEdit::editingFinished, this, &Slider2D::positionEdited);

    QGridLayout* layout = new QGridLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(display, 0, 0, 1, 4);
    layout->addWidget(new QLabel("Azimuth:"), 1, 0, 1, 1);
    layout->addWidget(xEdit, 1, 1, 1, 1);
    layout->addWidget(new QLabel("Elevation:"), 1, 2, 1, 1);
    layout->addWidget(yEdit, 1, 3, 1, 1);

    reset();
}

void Slider2D::reset()
{
    display->reset();

    updateText();
}

Sofa::Position Slider2D::getPosition() const
{
    return Sofa::Position(display->getX() * XScalingFactor, display->getY() * YScalingFactor, 0);
}

void Slider2D::setPosition(const Sofa::Position& position)
{
    if(!isInteractionEnabled)
    {
        display->setPosition(position.getAzimuth() / XScalingFactor, position.getElevation() / YScalingFactor);

        updateText();
    }
}

void Slider2D::positionEdited()
{
    bool xOk;
    double x = xEdit->text().toDouble(&xOk);

    bool yOk;
    double y = yEdit->text().toDouble(&yOk);

    if(xOk && yOk)
    {
        display->setPosition(x / XScalingFactor, y / YScalingFactor);

        emit positionChanged(getPosition());
    }

    updateText();
}

void Slider2D::positionMoved()
{
    updateText();

    emit positionChanged(getPosition());
}

void Slider2D::updateText()
{
    xEdit->setText(QString::number(getPosition().getAzimuth(), 'f', 2));
    yEdit->setText(QString::number(getPosition().getElevation(), 'f', 2));
}

Slider2DDisplay::Slider2DDisplay(QWidget* parent)
    : QFrame(parent)
{
    setFrameStyle(QFrame::Panel | QFrame::Sunken);
    setMinimumSize(100, 100);
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

void Slider2DDisplay::reset()
{
    x = 0;
    y = 0;

    update();
}

double Slider2DDisplay::getX() const
{
    return x;
}

double Slider2DDisplay::getY() const
{
    return y;
}

void Slider2DDisplay::setPosition(double x, double y)
{
    this->x = std::clamp(x, -1.0, 1.0);
    this->y = std::clamp(y, -1.0, 1.0);

    update();
}

void Slider2DDisplay::setDraggingEnabled(bool enabled)
{
    isDraggingEnabled = enabled;
}

void Slider2DDisplay::mouseMoveEvent(QMouseEvent* event)
{
    if(isDragging)
    {
        QPoint pos = event->pos() - center;

        setPosition(static_cast<double>(pos.x()) / scale.x(), static_cast<double>(-pos.y()) / scale.y());

        emit positionChanged(x, y);
    }
}

void Slider2DDisplay::mousePressEvent(QMouseEvent* event)
{
    if(isDraggingEnabled)
    {
        switch(event->button())
        {
        case Qt::LeftButton:
            isDragging = true;
            mouseMoveEvent(event);
            break;

        case Qt::RightButton:
            reset();
            emit positionChanged(x, y);
            break;

        default:
            break;
        }

        event->accept();
    }
    else
    {
        event->ignore();
    }
}

void Slider2DDisplay::mouseReleaseEvent(QMouseEvent* /*event*/)
{
    isDragging = false;
}

void Slider2DDisplay::paintEvent(QPaintEvent* event)
{
    QFrame::paintEvent(event);

    area.setTop(rect().top() + frameWidth());
    area.setBottom(rect().bottom() - frameWidth());
    area.setLeft(rect().left() + frameWidth());
    area.setRight(rect().right() - frameWidth());

    QPainter painter(this);
    painter.fillRect(area, Qt::white);

    center = QPoint(area.left() + area.width() / 2, area.top() + area.height() / 2);

    painter.setPen(Qt::gray);
    painter.drawEllipse(center, IconSize / 2, IconSize / 2);
    painter.drawLine(area.left() + IconSize / 2, area.top() + IconSize / 2, area.left() + IconSize, area.top() + IconSize / 2);
    painter.drawLine(area.left() + IconSize / 2, area.top() + IconSize / 2, area.left() + IconSize / 2, area.top() + IconSize);
    painter.drawLine(area.left() + IconSize / 2, area.bottom() - IconSize / 2, area.left() + IconSize, area.bottom() - IconSize / 2);
    painter.drawLine(area.left() + IconSize / 2, area.bottom() - IconSize / 2, area.left() + IconSize / 2, area.bottom() - IconSize);
    painter.drawLine(area.right() - IconSize / 2, area.top() + IconSize / 2, area.right() - IconSize, area.top() + IconSize / 2);
    painter.drawLine(area.right() - IconSize / 2, area.top() + IconSize / 2, area.right() - IconSize / 2, area.top() + IconSize);
    painter.drawLine(area.right() - IconSize / 2, area.bottom() - IconSize / 2, area.right() - IconSize, area.bottom() - IconSize / 2);
    painter.drawLine(area.right() - IconSize / 2, area.bottom() - IconSize / 2, area.right() - IconSize / 2, area.bottom() - IconSize);

    scale = QPoint((area.width() - IconSize) / 2, (area.height() - IconSize) / 2);

    QPoint icon = center + QPoint(x * scale.x(), -y * scale.y());

    painter.setPen(Qt::black);
    painter.drawLine(icon.x(), icon.y() - IconSize / 2, icon.x(), icon.y() + IconSize / 2);
    painter.drawLine(icon.x() - IconSize / 2, icon.y(), icon.x() + IconSize / 2, icon.y());
}
