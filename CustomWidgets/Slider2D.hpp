#pragma once

#include <QFrame>

#include "Sofa/Position.hpp"

class QLineEdit;
class Slider2DDisplay;

class Slider2D : public QWidget
{
    Q_OBJECT

public:
    Slider2D(bool interactionEnabled = false, QWidget* parent = nullptr);

    void reset();

    Sofa::Position getPosition() const;

    void setPosition(const Sofa::Position& position);

signals:
    void positionChanged(Sofa::Position position);

private slots:
    void positionEdited();
    void positionMoved();

private:
    static constexpr double XScalingFactor{-180};
    static constexpr double YScalingFactor{180};

    bool isInteractionEnabled{true};

    Slider2DDisplay* display;
    QLineEdit* xEdit;
    QLineEdit* yEdit;

    void updateText();
};

class Slider2DDisplay : public QFrame
{
    Q_OBJECT

public:
    Slider2DDisplay(QWidget* parent = nullptr);

    void reset();

    double getX() const;
    double getY() const;

    void setPosition(double x, double y);
    void setDraggingEnabled(bool enabled);

signals:
    void positionChanged(double x, double y);

private:
    static constexpr int IconSize = 10;

    double x{0};
    double y{0};

    bool isDragging{false};
    bool isDraggingEnabled{true};

    QPoint center;
    QPoint scale;
    QRect area;

    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void paintEvent(QPaintEvent* event) override;
};
