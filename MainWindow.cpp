#include "MainWindow.hpp"

#include <QGridLayout>
#include <QFileInfo>
#include <QCoreApplication>
#include <QTimer>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QApplication>
#include <QStatusBar>

#include "Utils/Duration.hpp"
#include "Utils/FileName.hpp"

#include "AudioManager.hpp"

#include "TrackerWindow.hpp"
#include "PlotWindow.hpp"

#include "CustomWidgets/InterfaceWidget.hpp"
#include "CustomWidgets/FileWidget.hpp"
#include "CustomWidgets/PositionWidget.hpp"
#include "CustomWidgets/OutputWidget.hpp"
#include "CustomWidgets/InputWidget.hpp"

#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setAcceptDrops(true);

    trackerWindow = new TrackerWindow(this);
    plotWindow = new PlotWindow(this);

    interfaceWidget = new InterfaceWidget;
    connect(interfaceWidget, &InterfaceWidget::startClicked, this, &MainWindow::start);

    fileWidget = new FileWidget;

    positionWidget = new PositionWidget;
    connect(positionWidget, &PositionWidget::positionChanged, this, &MainWindow::position);

    inputWidget = new InputWidget;
    connect(inputWidget, &InputWidget::command, this, &MainWindow::command);

    outputWidget = new OutputWidget;
    connect(outputWidget, &OutputWidget::levelChanged, this, &MainWindow::volume);

    QGridLayout* layout = new QGridLayout;
    layout->addWidget(interfaceWidget, 0, 0);
    layout->addWidget(fileWidget, 0, 1);
    layout->addWidget(positionWidget, 0, 2, 2, 1);
    layout->addWidget(inputWidget, 1, 0);
    layout->addWidget(outputWidget, 1, 1);

    QWidget* centralWidget = new QWidget;
    centralWidget->setLayout(layout);
    setCentralWidget(centralWidget);

    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MainWindow::timerUpdate);
    timer->start(20);

    statusBar()->showMessage("Ready");

    manualUpdate();
}

MainWindow::~MainWindow()
{}

void MainWindow::start()
{
    if(audioManager)
    {
        if(!audioManager->getErrorMessage().empty())
        {
            statusBar()->showMessage("Playback stopped: " + QString::fromStdString(audioManager->getErrorMessage()));
        }
        else
        {
            statusBar()->showMessage("Playback stopped");
        }

        audioManager->stop();
        audioManager->printTimer();
        audioManager.reset();
        Duration::reset();
    }
    else
    {
        statusBar()->showMessage("Preparing filters...");
        QCoreApplication::processEvents();

        audioManager = std::make_unique<AudioManager>(interfaceWidget->getSampleRate(), interfaceWidget->getBlockSize());

        if(enableTestSourcesAndFilters)
        {
            audioManager->loadTestSourcesAndFilters();
        }

        for(const auto& audioFile : fileWidget->getSourceFiles())
        {
            if(auto error = audioManager->addSource(audioFile))
            {
                std::cout << audioFile << ": " << *error << std::endl;
            }
        }

        for(const auto& sofaFile : fileWidget->getFilterFiles())
        {
            if(auto error = audioManager->addFilter(sofaFile))
            {
                std::cout << sofaFile << ": " << *error << std::endl;
            }
        }

        audioManager->setPosition(positionWidget->getPosition());
        audioManager->setOutputLevel(outputWidget->getLevel());

        if(auto error = audioManager->start(interfaceWidget->getDevice()))
        {
            std::cout << *error << std::endl;

            audioManager.reset();

            statusBar()->showMessage(QString::fromStdString(*error));
        }
        else
        {
            statusBar()->showMessage("Playback started");
        }
    }

    manualUpdate();
}

void MainWindow::position(Sofa::Position position)
{
    if(audioManager)
    {
        audioManager->setPosition(position);
    }
}

void MainWindow::command(PlaybackCommand command)
{
    if(audioManager)
    {
        audioManager->setPlaybackCommand(command);
    }
}

void MainWindow::volume(double value)
{
    if(audioManager)
    {
        audioManager->setOutputLevel(value);
    }
}

void MainWindow::manualUpdate()
{
    interfaceWidget->setEnabled(!audioManager);
    fileWidget->setEnabled(!audioManager);
    inputWidget->setEnabled(bool(audioManager));
    outputWidget->setEnabled(bool(audioManager));

    if(audioManager)
    {
        inputWidget->updateSources(audioManager->getSourceNames());
        inputWidget->updateFilters(audioManager->getFilterNames());
    }
}

void MainWindow::timerUpdate()
{
    Duration::Timer updateTimer("Fast update");

    if(audioManager && !audioManager->isRunning())
    {
        start();
    }

    if(audioManager)
    {
        inputWidget->updateStates(audioManager->getPlaybackStates(), interfaceWidget->getSampleRate());
        outputWidget->updatePeak(audioManager->getOutputPeak());
        outputWidget->updateCompression(std::vector<double>(1, audioManager->getOutputCompression()));
        plotWindow->setSpectrum(audioManager->getFilterSpectrum());
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    if (event->mimeData()->hasUrls())
    {
        bool supportedFiles = true;

        QList<QUrl> urls = event->mimeData()->urls();

        for(const auto& url : urls)
        {
            std::string path = url.toLocalFile().toStdString();
            std::string appendix = FileName::getAppendix(path);

            if(appendix != "sofa" && appendix != "mp3" && appendix != "wav")
            {
                supportedFiles = false;
            }
        }

        if(supportedFiles && !audioManager)
        {
            event->accept();
        }
    }
}

void MainWindow::dropEvent(QDropEvent* event)
{
    if (event->mimeData()->hasUrls())
    {
        QList<QUrl> urls = event->mimeData()->urls();

        for(const auto& url : urls)
        {
            std::string path = url.toLocalFile().toStdString();
            std::string appendix = FileName::getAppendix(path);

            if(appendix == "sofa")
            {
                fileWidget->addFilterFile(path);
            }
            else if(appendix == "mp3" || appendix == "wav")
            {
                fileWidget->addSourceFile(path);
            }
        }
    }
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if(event->modifiers().testFlag(Qt::ControlModifier))
    {
        switch(event->key())
        {
        case Qt::Key_S:
            start();
            break;

        case Qt::Key_T:
            enableTestSourcesAndFilters = !enableTestSourcesAndFilters;
            if(enableTestSourcesAndFilters)
            {
                statusBar()->showMessage("Test sources and filters enabled");
            }
            else
            {
                statusBar()->showMessage("Test sources and filters disabled");
            }
            break;

        case Qt::Key_U:
            trackerWindow->setVisible(!trackerWindow->isVisible());
            break;

        case Qt::Key_P:
            plotWindow->setVisible(!plotWindow->isVisible());
            break;

        default:
            break;
        }
    }
    else
    {
        switch(event->key())
        {
        case Qt::Key_Escape:
            if(QApplication::focusWidget())
            {
                QApplication::focusWidget()->clearFocus();
            }
            break;

        default:
            break;
        }
    }

    event->accept();
}
