#pragma once

#include <memory>

#include <QMainWindow>

#include "Sofa/Position.hpp"
#include "Utils/PlaybackCommand.hpp"

class AudioManager;

class InterfaceWidget;
class FileWidget;
class PositionWidget;
class OutputWidget;
class InputWidget;

class TrackerWindow;
class PlotWindow;

namespace Sofa { class Position; }

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void start();
    void position(Sofa::Position position);
    void command(PlaybackCommand command);
    void volume(double value);

private:
    void manualUpdate();
    void timerUpdate();

    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;
    void keyPressEvent(QKeyEvent* event) override;

    bool enableTestSourcesAndFilters{false};

    std::unique_ptr<AudioManager> audioManager;

    TrackerWindow* trackerWindow;
    PlotWindow* plotWindow;

    InterfaceWidget* interfaceWidget;
    FileWidget* fileWidget;
    PositionWidget* positionWidget;
    OutputWidget* outputWidget;
    InputWidget* inputWidget;
};
