#include "PlotWindow.hpp"

#include "CustomWidgets/LinePlot.hpp"
#include "Utils/LogSpectrum.hpp"

PlotWindow::PlotWindow(QWidget* parent)
    : QMainWindow(parent)
{
    plot = new LinePlot;
    plot->setContentsMargins(15, 15, 15, 15);

    setCentralWidget(plot);
}

void PlotWindow::setSpectrum(const LogSpectrum& data)
{
    plot->plot(data);
}

void PlotWindow::keyPressEvent(QKeyEvent* event)
{
    // Forward events to parent to access shortcuts
    parent()->event(static_cast<QEvent*>(event));
}
