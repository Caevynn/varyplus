#pragma once

#include <QMainWindow>

class LinePlot;
class LogSpectrum;

class PlotWindow : public QMainWindow
{
public:
    PlotWindow(QWidget *parent = nullptr);

    void setSpectrum(const LogSpectrum& data);

private:
    LinePlot* plot;

    void keyPressEvent(QKeyEvent* event) override;
};
