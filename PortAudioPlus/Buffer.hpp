#pragma once

#include <vector>
#include <cstring>
#include <cstdlib>
#include <cassert>

namespace PortAudioPlus
{

class Buffer
{
private:
    class RawDataHandler
    {
    public:
        virtual ~RawDataHandler() { reset(); }

        size_t getNumFrames() const { return nFrames; }
        size_t getNumChannels() const { return nChannels; }

        bool isValid() const { return nFrames != 0 && nChannels != 0 && data != nullptr; }

    protected:
        const float& getValue(size_t iFrame, size_t iChannel) const
        {
            return data[getIndex(iFrame, iChannel)];
        }

        void setValue(size_t iFrame, size_t iChannel, const float& value)
        {
            data[getIndex(iFrame, iChannel)] = value;
        }

    private:
        size_t nFrames = 0;
        size_t nChannels = 0;

        float* data = nullptr;

        // Only PortAudioPlus::Interface may resize buffer / reallocate memory (see private part of Buffer)
        friend class Buffer;

        void setSize(size_t nFrames, size_t nChannels)
        {
            reset();

            if(nChannels != 0 && nFrames != 0)
            {
                this->nFrames = nFrames;
                this->nChannels = nChannels;

                data = (float*) malloc(nFrames * nChannels * sizeof(float));
            }
        }

        void reset()
        {
            nFrames = 0;
            nChannels = 0;

            if(data != nullptr)
            {
                free(data);
                data = nullptr;
            }
        }

        size_t getIndex(size_t iFrame, size_t iChannel) const
        {
            assert(iFrame < nFrames);
            assert(iChannel < nChannels);

            return iFrame * nChannels + iChannel;
        }
    };

public:
    class Input : public RawDataHandler
    {
    public:
        void get(std::vector<std::vector<double>>& input) const
        {
            assert(input.size() == nChannels);

            for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
            {
                auto& channel = input[iChannel];

                assert(channel.size() == nFrames);

                for(size_t iFrame = 0; iFrame < nFrames; ++iFrame)
                {
                    channel[iFrame] = getValue(iFrame, iChannel);
                }
            }
        }

    private:
        // Only PortAudioPlus::Interface may get access raw data
        friend class Interface;

        bool setRawData(const void* source)
        {
            if(isValid())
            {
                memcpy(data, source, nFrames * nChannels * sizeof(float));

                return true;
            }
            else
            {
                return false;
            }
        }
    } input;

    class Output : public RawDataHandler
    {
    public:
        void set(const std::vector<std::vector<double>>& dataVector)
        {
            assert(dataVector.size() == nChannels);

            for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
            {
                assert(dataVector[iChannel].size() == nFrames);

                for(size_t iFrame = 0; iFrame < nFrames; ++iFrame)
                {
                    setValue(iFrame, iChannel, dataVector[iChannel][iFrame]);
                }
            }
        }

    private:
        // Only PortAudioPlus::Interface may get access raw data
        friend class Interface;

        bool getRawData(void* destination)
        {
            if(isValid())
            {
                memcpy(destination, data, nFrames * nChannels * sizeof(float));

                return true;
            }
            else
            {
                return false;
            }
        }
    } output;

private:
    // Only PortAudioPlus::Interface may resize buffer / reallocate memory
    friend class Interface;

    void setSize(size_t nFrames, size_t nInputChannels, size_t nOutputChannels)
    {
        input.setSize(nFrames, nInputChannels);
        output.setSize(nFrames, nOutputChannels);
    }
};

}
