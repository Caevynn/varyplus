#pragma once

#include <string>

#include "Buffer.hpp"

namespace PortAudioPlus
{

class CallbackHandler
{
public:
    virtual ~CallbackHandler() = default;

    void reset() { externalErrorFlags = 0; internalErrorFlags = 0; }
    void setExternalError(unsigned errorFlag) { externalErrorFlags |= errorFlag; }
    void setInternalError(unsigned errorFlag) { internalErrorFlags |= errorFlag; }

    virtual bool handleInput() { setInternalError(0x01); return false; }
    virtual bool handleOutput() { setInternalError(0x02); return false; }
    virtual bool handleDuplex() { setInternalError(0x04); return false; }
    virtual void handleFinished() {}

    std::string getErrorMessage() const
    {
        if(externalErrorFlags != 0)
        {
            return "External error: " + std::to_string(externalErrorFlags);
        }

        if(internalErrorFlags != 0)
        {
            return "Internal error: " + std::to_string(internalErrorFlags);
        }

        return "";
    }

    Buffer buffer;

private:
    unsigned int externalErrorFlags = 0, internalErrorFlags = 0;
};

}
