#pragma once

#include <algorithm>

#include "Interface.hpp"

namespace PortAudioPlus
{

class DeviceList
{
public:
    static std::vector<Device> getAvailableDevices()
    {
        Pa_Initialize();

        int nDevices = Pa_GetDeviceCount();

        std::vector<Device> deviceList;

        for(PaDeviceIndex iDevice = 0; iDevice < nDevices; ++iDevice)
        {
            deviceList.push_back(Device{iDevice, *Pa_GetDeviceInfo(iDevice)});
        }

        Pa_Terminate();

        return deviceList;
    }

    static std::vector<Device> getOutputDevices()
    {
        std::vector<Device> available = getAvailableDevices();
        std::vector<Device> outputDevices;

        std::copy_if(available.begin(), available.end(), std::back_inserter(outputDevices), [](const Device& device)
        {
            return device.info.maxOutputChannels > 0;
        });

        return outputDevices;
    }

    static std::vector<Device> getInputDevices()
    {
        std::vector<Device> available = getAvailableDevices();
        std::vector<Device> inputDevices;

        std::copy_if(available.begin(), available.end(), std::back_inserter(inputDevices), [](const Device& device)
        {
            return device.info.maxInputChannels > 0;
        });

        return inputDevices;
    }

    static Device getDefaultOutputDevice()
    {
        Pa_Initialize();
        PaDeviceIndex iDevice = Pa_GetDefaultOutputDevice();
        Device defaultOutput(iDevice, *Pa_GetDeviceInfo(iDevice));
        Pa_Terminate();

        return defaultOutput;
    }

    static Device getDefaultInputDevice()
    {
        Pa_Initialize();
        PaDeviceIndex iDevice = Pa_GetDefaultInputDevice();
        Device defaultInput(iDevice, *Pa_GetDeviceInfo(iDevice));
        Pa_Terminate();

        return defaultInput;
    }
};

}
