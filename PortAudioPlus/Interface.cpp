#include "Interface.hpp"

#include <algorithm>

#define ABORT_ON_FAIL(x) if(!(x)) return paAbort

int PortAudioPlus::Interface::audioCallback(
        const void *inputBuffer,
        void* outputBuffer,
        unsigned long framesPerBuffer,
        const PaStreamCallbackTimeInfo* /*timeInfo*/,
        PaStreamCallbackFlags statusFlags,
        void *userData)
{
    PortAudioPlus::CallbackHandler* handler = static_cast<PortAudioPlus::CallbackHandler*>(userData);

    // Check status
    if(statusFlags != 0)
    {
        handler->setExternalError(statusFlags);
        return paAbort;
    }

    if(handler->buffer.input.isValid())
    {
        ABORT_ON_FAIL(framesPerBuffer == handler->buffer.input.getNumFrames());
        ABORT_ON_FAIL(handler->buffer.input.setRawData(inputBuffer));

        if(handler->buffer.output.isValid())
        {
            ABORT_ON_FAIL(framesPerBuffer == handler->buffer.output.getNumFrames());
            ABORT_ON_FAIL(handler->handleDuplex());
            ABORT_ON_FAIL(handler->buffer.output.getRawData(outputBuffer));
        }
        else
        {
            ABORT_ON_FAIL(handler->handleInput());
        }
    }
    else
    {
        ABORT_ON_FAIL(framesPerBuffer == handler->buffer.output.getNumFrames());
        ABORT_ON_FAIL(handler->handleOutput());
        ABORT_ON_FAIL(handler->buffer.output.getRawData(outputBuffer));
    }

    return paContinue;
}

void PortAudioPlus::Interface::finishedCallback(void *userData)
{
    PortAudioPlus::CallbackHandler* handler = static_cast<PortAudioPlus::CallbackHandler*>(userData);

    handler->handleFinished();
}

PortAudioPlus::Interface::Interface()
{
    Pa_Initialize();
}

PortAudioPlus::Interface::~Interface()
{
    Pa_CloseStream(paStream);
    Pa_Terminate();
}

bool PortAudioPlus::Interface::start(Stream stream, CallbackHandler& handler)
{
    if(Pa_IsStreamActive(paStream) == 1)
    {
        return false;
    }

    mode = stream.mode;

    switch(mode)
    {
    case Mode::Record:
    {
        PaStreamParameters inParam;

        if(!getInputParameters(stream, inParam))
        {
            return false;
        }

        handler.buffer.setSize(stream.framesPerBuffer, stream.numChannels, 0);

        PaError error = Pa_OpenStream(
                    &paStream,
                    &inParam,
                    nullptr,
                    stream.sampleRate,
                    stream.framesPerBuffer,
                    paClipOff,
                    audioCallback,
                    &handler);

        if(error != paNoError)
        {
            return false;
        }
    }
    break;

    case Mode::Play:
    {
        PaStreamParameters outParam;

        if(!getOutputParameters(stream, outParam))
        {
            return false;
        }

        handler.buffer.setSize(stream.framesPerBuffer, 0, stream.numChannels);

        PaError error = Pa_OpenStream(
                    &paStream,
                    nullptr,
                    &outParam,
                    stream.sampleRate,
                    stream.framesPerBuffer,
                    paClipOff,
                    audioCallback,
                    &handler);

        if(error != paNoError)
        {
            return false;
        }
    }
    break;

    case Mode::Duplex:
    {
        PaStreamParameters inParam, outParam;

        if(!getOutputParameters(stream, outParam))
        {
            return false;
        }

        if(!getInputParameters(stream, inParam))
        {
            return false;
        }

        handler.buffer.setSize(stream.framesPerBuffer, stream.numChannels, stream.numChannels);

        PaError error = Pa_OpenStream(
                    &paStream,
                    &inParam,
                    &outParam,
                    stream.sampleRate,
                    stream.framesPerBuffer,
                    paClipOff,
                    audioCallback,
                    &handler);

        if(error != paNoError)
        {
            return false;
        }
    }
    break;
    }

    if(Pa_SetStreamFinishedCallback(paStream, finishedCallback) != paNoError)
    {
        stop();

        return false;
    }

    if(Pa_StartStream(paStream) != paNoError)
    {
        stop();

        return false;
    }

    return true;
}

bool PortAudioPlus::Interface::stop()
{
    // This function blocks untill playback is finished

    if(Pa_CloseStream(paStream) != paNoError)
    {
        return false;
    }

    return true;
}

bool PortAudioPlus::Interface::getInputParameters(Stream stream, PaStreamParameters& inParam) const
{
    if(stream.numChannels > stream.device.info.maxOutputChannels)
    {
        return false;
    }

    inParam.device = stream.device.index;
    inParam.channelCount = stream.numChannels;
    inParam.sampleFormat = paFloat32;
    inParam.suggestedLatency = stream.device.info.defaultLowInputLatency;
    inParam.hostApiSpecificStreamInfo = nullptr;

    return true;
}

bool PortAudioPlus::Interface::getOutputParameters(Stream stream, PaStreamParameters& outParam) const
{
    if(stream.numChannels > stream.device.info.maxOutputChannels)
    {
        return false;
    }

    outParam.device = stream.device.index;
    outParam.channelCount = stream.numChannels;
    outParam.sampleFormat = paFloat32;
    outParam.suggestedLatency = stream.device.info.defaultLowOutputLatency;
    outParam.hostApiSpecificStreamInfo = nullptr;

    return true;
}
