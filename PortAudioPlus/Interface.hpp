#pragma once

#include <string>

#include "portaudio.h"

#include "CallbackHandler.hpp"

namespace PortAudioPlus
{

enum class Mode
{
    Record,
    Play,
    Duplex
};

struct Device
{
    Device(PaDeviceIndex index, PaDeviceInfo info)
        : index(index), info(info), name(info.name)
    {}

    Device() {}

    PaDeviceIndex index;
    PaDeviceInfo info;
    std::string name;
};

struct Stream
{
    Device device;
    Mode mode;
    int sampleRate;
    int numChannels;
    int framesPerBuffer;
};

class Interface
{
public:
    Interface();
    ~Interface();

    bool start(Stream stream, CallbackHandler& handler);
    bool stop();

    Mode getMode() const { return mode; }
    bool isRunning() const { return Pa_IsStreamActive(paStream) == 1; }

private:
    PaStream* paStream = nullptr;
    Mode mode;

    bool getInputParameters(Stream stream, PaStreamParameters& inParam) const;
    bool getOutputParameters(Stream stream, PaStreamParameters& outParam) const;

    static int audioCallback(
            const void *inputBuffer,
            void* outputBuffer,
            unsigned long framesPerBuffer,
            const PaStreamCallbackTimeInfo* timeInfo,
            PaStreamCallbackFlags statusFlags,
            void *userData);

    static void finishedCallback(void* userData);
};

}
