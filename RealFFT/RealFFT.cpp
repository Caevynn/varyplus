#include "RealFFT.hpp"

#include <cassert>

RealFFT::RealFFT(size_t length) :
    nFFT(length / 2)
{
    table.twiddleFactor.reserve(nFFT / 2);

    for(size_t i = 0; i < nFFT / 2; ++i)
    {
        double angle = 2.0 * M_PI * static_cast<double>(i) / static_cast<double>(nFFT);

        table.twiddleFactor.push_back(Complex(std::cos(angle), -std::sin(angle)));
    }

    table.cosHalf.reserve(nFFT / 2);

    for(size_t i = 0; i < nFFT / 2; ++i)
    {
        double angle = M_PI * static_cast<double>(i) / static_cast<double>(nFFT);

        table.cosHalf.push_back(std::cos(angle));
    }
}

void RealFFT::forward(const Signal& signal, Spectrum& spectrum) const
{
    assert(signal.size() == 2 * nFFT);
    assert(spectrum.size() == nFFT + 1);

    for(size_t i = 0; i < nFFT; ++i)
    {
        spectrum[i] = Complex(signal[2 * i + 0], signal[2 * i + 1]);
    }

    spectrum[nFFT] = 0;

    core(spectrum);

    forwardPostprocessing(spectrum);
}

void RealFFT::forwardWithZeropadding(const Signal& signal, Spectrum& spectrum) const
{
    assert(signal.size() == nFFT);
    assert(spectrum.size() == nFFT + 1);

    for(size_t i = 0; i < nFFT / 2; ++i)
    {
        spectrum[i] = Complex(signal[2 * i + 0], signal[2 * i + 1]);
    }

    for(size_t i = nFFT / 2; i < nFFT + 1; ++i)
    {
        spectrum[i] = 0;
    }

    core(spectrum);

    forwardPostprocessing(spectrum);
}

void RealFFT::inverse(Spectrum& spectrum, Signal& signal) const
{
    assert(spectrum.size() == nFFT + 1);
    assert(signal.size() == 2 * nFFT);

    inversePreprocessing(spectrum);

    core(spectrum);

    double norm = 1.0 / static_cast<double>(nFFT * 2);

    for(size_t i = 0; i < nFFT; ++i)
    {
        signal[2 * i] = spectrum[i].real() * norm;
        signal[2 * i + 1] = spectrum[i].imag() * norm;
    }
}

void RealFFT::forwardPostprocessing(Spectrum& spectrum) const
{
    Complex tmp = spectrum[0];

    spectrum[0] = {tmp.real() + tmp.imag(), 0};
    spectrum[nFFT] = {tmp.real() - tmp.imag(), 0};
    spectrum[nFFT / 2] = std::conj(spectrum[nFFT / 2]);

    for(size_t i = 1; i < nFFT / 2; ++i)
    {
        size_t j = nFFT - i;

        Complex& xi = spectrum[i];
        Complex& xj = spectrum[j];

        double rs = (xi.real() + xj.real()) * 0.5;
        double is = (xi.imag() + xj.imag()) * 0.5;
        double rd = (xj.real() - xi.real()) * 0.5;
        double id = (xi.imag() - xj.imag()) * 0.5;

        double ci = table.cosHalf[i];
        double cj = table.cosHalf[nFFT / 2 - i];

        double rp = is * ci + rd * cj;
        double ip = rd * ci - is * cj;

        xi.real(rs + rp);
        xi.imag(ip + id);

        xj.real(rs - rp);
        xj.imag(ip - id);
    }
}

void RealFFT::inversePreprocessing(Spectrum& spectrum) const
{
    double t0 = spectrum[0].real();
    double tn = spectrum[nFFT].real();

    spectrum[0].real(t0 + tn);
    spectrum[0].imag(t0 - tn);

    Complex tmp = spectrum[nFFT / 2];

    spectrum[nFFT / 2].real(tmp.real() * 2);
    spectrum[nFFT / 2].imag(tmp.imag() * 2);

    for(size_t i = 1; i < nFFT / 2; ++i)
    {
        size_t j = nFFT - i;

        double rs = (spectrum[i].real() + spectrum[j].real());
        double is = (spectrum[i].imag() + spectrum[j].imag());

        double rd = (spectrum[i].real() - spectrum[j].real());
        double id = (spectrum[i].imag() - spectrum[j].imag());

        double ci = table.cosHalf.at(i);
        double cj = table.cosHalf.at(nFFT / 2 - i);

        double rp = is * ci + rd * cj;
        double ip = rd * ci - is * cj;

        spectrum[i].real(rp + rs);
        spectrum[j].real(rs - rp);

        spectrum[i].imag(ip - id);
        spectrum[j].imag(ip + id);
    }
}

void RealFFT::core(Spectrum& values) const
{
    // Reorder values according to bit reversal

    for(int i = 0, j = 0; i < static_cast<int>(nFFT - 1); ++i)
    {
        if(i < j)
        {
            Complex tmp = values[j];
            values[j] = values[i];
            values[i] = tmp;
        }

        int k = nFFT / 2;

        while(k <= j)
        {
            j -= k;
            k /= 2;
        }

        j += k;
    }

    // First stage

    for(int iGroup = 0, i = 0, j = 1; iGroup < static_cast<int>(nFFT / 2); ++iGroup)
    {
        Complex tmp = values[j];

        values[j] = values[i] - tmp;
        values[i] = values[i] + tmp;

        i += 2;
        j += 2;
    }

    // Butterfly

    for(int nGroups = nFFT / 4, nButterflies = 2; nGroups > 0; nGroups /= 2)
    {
        int i = 0;
        int j = nButterflies;

        for(int iGroup = 0; iGroup < nGroups; ++iGroup)
        {
            for(int l = 0, k = 0; l < nButterflies; ++l)
            {
                Complex& xi = values[i];
                Complex& xj = values[j];
                const Complex& wk = table.twiddleFactor.at(k);

                const Complex tmp = {
                    xj.real() * wk.real() - xj.imag() * wk.imag(),
                    xj.imag() * wk.real() + xj.real() * wk.imag()};

                xj = xi - tmp;
                xi = xi + tmp;

                k += nGroups;

                ++i;
                ++j;
            }

            i += nButterflies;
            j += nButterflies;
        }

        nButterflies *= 2;
    }
}
