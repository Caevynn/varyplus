#pragma once

#include <complex>
#include <vector>

class RealFFT
{
    using Complex = std::complex<double>;
    using Spectrum = std::vector<Complex>;
    using Signal = std::vector<double>;

public:
    RealFFT(size_t length);

    void forward(const Signal& input, Spectrum& output) const;
    void forwardWithZeropadding(const Signal& input, Spectrum& output) const;
    void inverse(Spectrum& input, Signal& output) const;

private:
    struct Table
    {
        std::vector<Complex> twiddleFactor;
        std::vector<double> cosHalf;
    } table;

    size_t nFFT;

    void forwardPostprocessing(Spectrum& spectrum) const;
    void inversePreprocessing(Spectrum& spectrum) const;
    void core(Spectrum& values) const;
};
