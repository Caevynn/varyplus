#include "File.hpp"

#include <algorithm>

#include "SOFASimpleFreeFieldHRIR.h"

namespace Sofa
{

File::File(const std::string& filename)
    : filename(filename)
{
    valid = load(filename);
}

bool File::load(const std::string &filename)
{
    try
    {
        const sofa::SimpleFreeFieldHRIR sofaFile(filename);

        if(!loadMetaData(sofaFile))
        {
            errorMsg = "SofaFile: loading meta data failed";
            return false;
        }

        if(!loadSampleRate(sofaFile))
        {
            errorMsg = "SofaFile: loading sample rate failed";
            return false;
        }

        if(!loadPositions(sofaFile) || positionList.size() != nMeasurements)
        {
            errorMsg = "SofaFile: loading positions failed";
            return false;
        }

        if(!loadImpulseResponses(sofaFile) || impulseResponseList.size() != nMeasurements)
        {
            errorMsg = "SofaFile: loading impulse responses failed";
            return false;
        }
    }
    catch(...)
    {
        errorMsg = "SofaFile: Could not open " + filename;
        return false;
    }

    return true;
}

bool File::loadMetaData(const sofa::SimpleFreeFieldHRIR &sofaFile)
{
    nChannels = sofaFile.GetNumReceivers();
    nMeasurements = sofaFile.GetNumMeasurements();
    nSamples = sofaFile.GetNumDataSamples();

    return nChannels != 0 && nMeasurements != 0 && nSamples != 0;
}

bool File::loadSampleRate(const sofa::SimpleFreeFieldHRIR &sofaFile)
{
    if(double tmp; sofaFile.GetSamplingRate(tmp))
    {
        sampleRate = static_cast<size_t>(tmp);
    }

    return sampleRate != 0;
}

bool File::loadPositions(const sofa::SimpleFreeFieldHRIR &sofaFile)
{
    if(nMeasurements == 1)
    {
        positionList.push_back(Position(0, 0, 0));
        return true;
    }

    size_t nExpectedValues = nMeasurements * Position::NumDimensions;

    if(nExpectedValues == 0)
    {
        throw std::runtime_error("SofaFile::loadPositions: nExpectedValues == 0");
    }

    if(std::vector<double> positions; sofaFile.GetListenerPosition(positions) && positions.size() == nExpectedValues)
    {
        for(auto it = positions.begin(); it != positions.end();)
        {
            positionList.push_back(Position::Listener(*it++, *it++, *it++));
        }

        return true;
    }

    if(std::vector<double> positions; sofaFile.GetSourcePosition(positions) && positions.size() == nExpectedValues)
    {
        for(auto it = positions.begin(); it != positions.end();)
        {
            positionList.push_back(Position::Source(*it++, *it++, *it++));
        }

        return true;
    }

    if(std::vector<double> positions; sofaFile.GetListenerView(positions) && positions.size() == nExpectedValues)
    {
        for(auto it = positions.begin(); it != positions.end();)
        {
            positionList.push_back(Position::Source(*it++, *it++, *it++));
        }

        return true;
    }

    return false;
}

bool File::loadImpulseResponses(const sofa::SimpleFreeFieldHRIR &sofaFile)
{
    size_t nExpectedSamples = nMeasurements * nChannels * nSamples;

    if(nExpectedSamples == 0)
    {
        throw std::runtime_error("SofaFile::loadImpulseResponses: nExpectedSamples == 0");
    }

    std::vector<double> data;

    if(!sofaFile.GetDataIR(data) || data.size() != nExpectedSamples)
    {
        return false;
    }

    auto dataIt = data.begin();

    for(size_t iMeasurement = 0; iMeasurement < nMeasurements; ++iMeasurement)
    {
        ImpulseResponse impulseResponse;

        for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
        {
            std::vector<double> values;
            values.reserve(nSamples);

            std::copy_n(dataIt, nSamples, std::back_inserter(values));

            impulseResponse.addChannel(values);

            dataIt += nSamples;
        }

        impulseResponseList.push_back(impulseResponse);
    }

    return true;
}

} // namespace Sofa
