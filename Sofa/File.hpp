#pragma once

#include <vector>
#include <string>

#include "Position.hpp"

#include "Utils/ImpulseResponse.hpp"

namespace sofa
{
    class SimpleFreeFieldHRIR;
}

namespace Sofa
{

class File
{
public:
    File(const std::string& filename);

    bool isValid() const { return valid; }
    const std::string& getFilename() const { return filename; }
    const std::string& getErrorMsg() const { return errorMsg; }
    size_t getNumChannels() const { return nChannels; }
    size_t getNumSamples() const { return nSamples; }
    size_t getNumMeasurements() const { return nMeasurements; }
    size_t getSampleRate() const { return sampleRate; }
    const std::vector<ImpulseResponse>& getImpulseResponseList() const { return impulseResponseList; }
    const std::vector<Position>& getPositionList() const { return positionList; }

private:
    bool valid;
    std::string filename;
    std::string errorMsg;

    size_t nSamples = 0, nMeasurements = 0, nChannels = 0, sampleRate = 0;

    std::vector<ImpulseResponse> impulseResponseList;
    std::vector<Position> positionList;

    bool load(const std::string& filename);
    bool loadMetaData(const sofa::SimpleFreeFieldHRIR& sofaFile);
    bool loadSampleRate(const sofa::SimpleFreeFieldHRIR& sofaFile);
    bool loadPositions(const sofa::SimpleFreeFieldHRIR& sofaFile);
    bool loadImpulseResponses(const sofa::SimpleFreeFieldHRIR& sofaFile);
};

} // namespace Sofa
