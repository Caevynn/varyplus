#include "Filter.hpp"

#include "Position.hpp"
#include "File.hpp"

#include "Utils/FileName.hpp"

namespace Sofa
{

Filter::Filter(const File& sofaFile, size_t blockSize, std::shared_ptr<Position> position)
    : TransferFunctionBase(blockSize, FileName::removePath(sofaFile.getFilename())),
      position(position)
{
    if(!sofaFile.isValid())
    {
        throw std::runtime_error("SofaFilter::load - Invalid sofa file.");
    }

    for(const auto& impulseResponse : sofaFile.getImpulseResponseList())
    {
        filterList.push_back(TransferFunction(impulseResponse, blockSize, sofaFile.getSampleRate(), ""));
    }

    positionList = sofaFile.getPositionList();
}

void Filter::apply(const OverlapAddBuffer& input, std::vector<Spectrum>& output) const
{
    filterList[getPositionIndex(*position)].apply(input, output);
}

const LogSpectrum& Filter::getLogSpectrum() const
{
    return filterList[getPositionIndex(*position)].getLogSpectrum();
}

size_t Filter::getPositionIndex(const Position& position) const
{
    double minDistance = positionList[0].distance(position);
    size_t nearestPositionIndex = 0;

    for(size_t i = 1; i < positionList.size(); ++i)
    {
        double currentDistance = positionList[i].distance(position);

        if(currentDistance < minDistance)
        {
            minDistance = currentDistance;
            nearestPositionIndex = i;
        }
    }

    return nearestPositionIndex;
}

} // namespace Sofa
