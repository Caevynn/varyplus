#pragma once

#include <vector>
#include <memory>

#include "Utils/TransferFunctionBase.hpp"
#include "Utils/TransferFunction.hpp"

namespace Sofa
{
class Position;
class File;

class Filter : public TransferFunctionBase
{
    using Spectrum = std::vector<std::complex<double>>;

public:
    Filter(const File& sofaFile, size_t blockSize, std::shared_ptr<Position> position);

    void apply(const OverlapAddBuffer& input, std::vector<Spectrum>& output) const override;

    size_t getNumPartitions() const override { return filterList.front().getNumPartitions(); }
    size_t getSpectrumSize() const override { return filterList.front().getSpectrumSize(); }
    size_t getNumChannels() const override { return filterList.front().getNumChannels(); }

    const LogSpectrum& getLogSpectrum() const override;

private:
    std::vector<TransferFunction> filterList;
    std::vector<Position> positionList;

    std::shared_ptr<Position> position;

    size_t getPositionIndex(const Position& position) const;
};

} // namespace Sofa
