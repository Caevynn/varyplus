#pragma once

#include <cmath>

namespace Sofa
{

class Position
{
public:
    Position(double azimuth, double elevation, double radius) :
        azimuth(adjustDegree(azimuth)),
        elevation(adjustDegree(elevation)),
        radius(radius)
    {}

    static Position Listener(double radius, double elevation, double azimuth)
    {
        return Position(azimuth, elevation, radius);
    }

    static Position Source(double radius, double elevation, double azimuth)
    {
        return Position(-azimuth, -elevation, radius);
    }

    double distance(const Position& other) const
    {
        return std::sqrt(
                    std::pow(azimuth - other.azimuth, 2) +
                    std::pow(elevation - other.elevation, 2) +
                    std::pow(radius - other.radius, 2));
    }

    double getAzimuth() const { return azimuth; }
    double getElevation() const { return elevation; }
    double getRadius() const { return radius; }

    Position operator+(const Position& other) const
    {
        return Position(azimuth + other.azimuth, elevation + other.elevation, radius + other.radius);
    }

    Position operator-(const Position& other) const
    {
        return Position(azimuth - other.azimuth, elevation - other.elevation, radius - other.radius);
    }

    static constexpr size_t NumDimensions = 3;

private:
    double azimuth, elevation, radius;

    double adjustDegree(double value)
    {
        if(value > 180) return value - 360;
        else if(value < -180) return value + 360;
        else return value;
    }
};

} // namespace Sofa
