#pragma once

#include <vector>
#include <string>

#include "Settings.hpp"

namespace SoundFilePlus
{

enum class Mode
{
    Read,
    Write,
    Invalid
};

class Base
{
public:
    virtual ~Base() = default;

    virtual Settings open(const std::string& fileName, Mode mode, Settings settings = Settings()) = 0;
    virtual void close() = 0;
    virtual void reset() = 0;

    virtual const std::vector<std::vector<double>> read(size_t frames) = 0;
    virtual bool write(const std::vector<std::vector<double>>& /*input*/) { return false; };

    const std::string& getErrorMessage() const { return errorMessage; }
    const std::string& getFileName() const { return fileName; }
protected:
    Mode mode = Mode::Invalid;

    size_t nChannels;

    std::string errorMessage;
    std::string fileName;
};

}
