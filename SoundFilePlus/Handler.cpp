#include "Handler.hpp"

#include "WAV.hpp"
#include "MP3.hpp"

#include <memory>

SoundFilePlus::Handler::Handler(const std::string& fileName, Mode mode, Settings writeSettings)
{
    switch(getFileType(fileName))
    {
    case FileType::WAV:
        file = std::make_unique<WAV>();
        break;

    case FileType::MP3:
        file = std::make_unique<MP3>();
        break;

    case FileType::Other:
        errorMessage = "SoundFilePlus::Handler::open: File format not supported: " + getAppendix(fileName) + " (only mp3 and wav are supported)";
        return;
    }

    audioSettings = file->open(fileName, mode, writeSettings);

    if(!audioSettings.isValid())
    {
        errorMessage = file->getErrorMessage();
        file.reset();
    }
}

SoundFilePlus::Settings SoundFilePlus::Handler::open(const std::string& fileName, Mode mode, Settings writeSettings)
{
    if(file)
    {
        return Settings();
    }

    switch(getFileType(fileName))
    {
    case FileType::WAV:
        file = std::make_unique<WAV>();
        break;

    case FileType::MP3:
        file = std::make_unique<MP3>();
        break;

    case FileType::Other:
        errorMessage = "SoundFilePlus::Handler::open: File format not supported: " + getAppendix(fileName) + " (only mp3 and wav are supported)";
        return Settings();
    }

    audioSettings = file->open(fileName, mode, writeSettings);

    if(!audioSettings.isValid())
    {
        errorMessage = file->getErrorMessage();
        file.reset();
        return Settings();
    }

    return audioSettings;
}

void SoundFilePlus::Handler::close()
{
    if(file)
    {
        file->close();
        file.reset();
    }
}

void SoundFilePlus::Handler::reset()
{
    if(file)
    {
        file->reset();
    }
}

std::vector<double> SoundFilePlus::Handler::readMono(size_t frames)
{
    std::vector<std::vector<double>> data = read(frames);

    std::vector<double> output;
    output.reserve(data[0].size());

    if(data.size() > 0 && data[0].size() > 0)
    {
        size_t nChannels = data.size();
        size_t nFrames = data[0].size();

        for(size_t iFrame = 0; iFrame < nFrames; ++iFrame)
        {
            double sum = 0;

            for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
            {
                sum += data[iChannel][iFrame];
            }

            output.push_back(sum / nChannels);
        }
    }

    return output;
}

std::vector<std::vector<double>> SoundFilePlus::Handler::read(size_t frames)
{
    return file ? file->read(frames) : std::vector<std::vector<double>>();
}

bool SoundFilePlus::Handler::write(const std::vector<std::vector<double>>& input)
{
    return file ? file->write(input) : false;
}
