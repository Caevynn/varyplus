#pragma once

#include <memory>

#include "Base.hpp"
#include "Settings.hpp"

namespace SoundFilePlus
{

class Handler
{
public:
    Handler() = default;
    Handler(const std::string& fileName, Mode mode, Settings writeSettings = Settings());

    Settings open(const std::string& fileName, Mode mode, Settings writeSettings = Settings());
    void close();

    // Source functions
    void reset();
    std::vector<std::vector<double>> read(size_t frames);
    std::vector<double> readMono(size_t frames);

    // Sink functions
    bool write(const std::vector<std::vector<double>>& input);

    // Joint source / sink functions
    Settings getSettings() const { return audioSettings; }

    const std::string& getErrorMessage() const { return file ? file->getErrorMessage() : errorMessage; }
    const std::string& getFileName() const { return file ? file->getFileName() : errorMessage; }

private:
    std::unique_ptr<Base> file; // Base is a joint interface for WAV and MP3

    std::string errorMessage = "No file opened.";

    Settings audioSettings;

    enum class FileType
    {
        WAV,
        MP3,
        Other
    };

    FileType getFileType(const std::string& fileName) const
    {
        std::string appendix = getAppendix(fileName);

        if(appendix == "mp3" || appendix == "MP3")
        {
            return FileType::MP3;
        }
        else if(appendix == "wav" || appendix == "WAV")
        {
            return FileType::WAV;
        }
        else
        {
            return FileType::Other;
        }
    }

    std::string getAppendix(const std::string& fileName) const
    {
        return fileName.substr(fileName.find_last_of(".") + 1);
    }
};

}
