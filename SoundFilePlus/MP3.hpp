#pragma once

#include <stdexcept>

#include "Base.hpp"

#include <mpg123.h>

namespace SoundFilePlus
{

class MP3 : public Base
{
public:
    ~MP3()
    {
        close();
    }

    Settings open(const std::string& fileName, Mode mode, Settings /*writeSettings*/ = Settings()) override
    {
        if(handle != nullptr || this->mode != Mode::Invalid)
        {
            return Settings();
        }

        errorMessage = "";
        this->fileName = fileName;

        static bool isMpg123Initialized = false;

        if(!isMpg123Initialized)
        {
            int error = mpg123_init();

            if(error != MPG123_OK)
            {
                errorMessage = std::string("MP3::open - mpg123_init: ") + std::string(mpg123_plain_strerror(error));
                return Settings();
            }

            isMpg123Initialized = true;
        }

        switch(mode)
        {
        case Mode::Read:
        {
            int error = MPG123_OK;

            handle = mpg123_new(NULL, &error);

            if(error != MPG123_OK)
            {
                errorMessage = std::string("MP3::open - Mode::Read(0): ") + std::string(mpg123_plain_strerror(error));
                return Settings();
            }

            error = mpg123_open(handle, (fileName).c_str());

            if(error != MPG123_OK)
            {
                errorMessage = std::string("MP3::open - Mode::Read(1): ") + std::string(mpg123_plain_strerror(error));
                return Settings();
            }

            long int samplerate;
            int channels, encoding;

            error = mpg123_getformat(handle, &samplerate, &channels, &encoding);

            if(error != MPG123_OK)
            {
                errorMessage = std::string("MP3::open - Mode::Read(2): ") + std::string(mpg123_plain_strerror(error));
                return Settings();
            }

            int frames = mpg123_length(handle);

            mode = Mode::Read;

            nChannels = channels;

            return Settings(samplerate, frames, channels);
        }

        case Mode::Write:
            errorMessage = std::string("MP3::open - Mode::Write: Write not supported.");
            return Settings();

        default:
            errorMessage = std::string("MP3::open - Invalid mode.");
            return Settings();
        }
    }

    void close() override
    {
        if(mode != Mode::Invalid)
        {
            mpg123_close(handle);

            handle = nullptr;

            mode = Mode::Invalid;
        }
    }

    void reset() override
    {
        mpg123_seek(handle, 0, SEEK_SET);
    }

    const std::vector<std::vector<double>> read(size_t frames) override
    {
        size_t nValues = frames * nChannels;
        std::vector<int16_t> input(nValues);

        size_t nBytes = input.size() * sizeof(int16_t);
        std::vector<unsigned char> byteBuffer(nBytes);

        size_t nBytesRead;
        int error = mpg123_read(handle, byteBuffer.data(), nBytes, &nBytesRead);
        size_t nFramesRead = nBytesRead / sizeof(int16_t) / nChannels;

        if(error != MPG123_OK)
        {
            errorMessage = std::string("MP3::read: ") + std::string(mpg123_plain_strerror(error));
            return std::vector<std::vector<double>>();
        }

        memcpy(input.data(), byteBuffer.data(), nBytesRead);

        std::vector<std::vector<double>> output;
        output.reserve(nChannels);

        for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
        {
            std::vector<double> channel;
            channel.reserve(nFramesRead);

            for(size_t iFrame = 0; iFrame < nFramesRead; ++iFrame)
            {
                size_t index = iFrame * nChannels + iChannel;
                channel.push_back(static_cast<double>(input[index]) / 0x8000);
            }

            output.push_back(channel);
        }

        return output;
    }

private:
    mpg123_handle* handle = nullptr;
};

}
