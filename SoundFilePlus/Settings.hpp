#pragma once

#include <vector>
#include <string>

namespace SoundFilePlus
{

class Settings
{
public:
    Settings(size_t sampleRate, size_t nFrames, size_t nChannels) :
        valid(true),
        sampleRate(sampleRate),
        nFrames(nFrames),
        nChannels(nChannels)
    {}

    Settings() = default;

    bool isValid() const { return valid; }
    size_t getSampleRate() const { return sampleRate; }
    size_t getFrames() const { return nFrames; }
    size_t getChannels() const { return nChannels; }

private:
    bool valid = false;
    size_t sampleRate = 0;
    size_t nFrames = 0;
    size_t nChannels = 0;
};

} // namespace SoundFilePlus
