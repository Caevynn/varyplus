#pragma once

#include <string>
#include <algorithm>
#include <cassert>

#include "Utils/SourceBase.hpp"
#include "Utils/FileName.hpp"
#include "Handler.hpp"

namespace SoundFilePlus
{

class Source : public SourceBase
{
    using Signal = std::vector<double>;

public:
    Source(Handler& file)
        : SourceBase(FileName::removePath(file.getFileName()))
    {
        if(!file.getSettings().isValid())
        {
            throw std::runtime_error("File not valid");
        }

        file.reset();
        data = file.readMono(file.getSettings().getFrames());
    }

    void reset() override
    {
        position = 0;
    }

    void read(size_t position, size_t nSamples, Signal& output) override
    {
        setPosition(position);
        read(nSamples, output);
    }

    void read(size_t nSamples, Signal& output) override
    {
        assert(output.size() == nSamples);

        size_t remainingSamples = data.size() - position;
        size_t samplesToCopy = std::min(nSamples, remainingSamples);
        std::copy_n(data.begin() + position, samplesToCopy, output.begin());
        std::fill_n(output.begin() + samplesToCopy, nSamples - samplesToCopy, 0);

        position += samplesToCopy;
    }

    void setPosition(size_t position) override
    {
        this->position = position;
    }

    size_t getPosition() const override
    {
        return position;
    }

    size_t getLength() const override
    {
        return data.size();
    }

private:
    Signal data;

    size_t position{0};
};

} // namespace SoundFilePlus
