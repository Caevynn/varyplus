#pragma once

#include <stdexcept>

#include "Base.hpp"

#include <sndfile.h>

namespace SoundFilePlus
{

class WAV : public Base
{
public:
    ~WAV()
    {
        close();
    }

    Settings open(const std::string& fileName, Mode mode, Settings writeSettings = Settings()) override
    {
        if(handle != nullptr || this->mode != Mode::Invalid)
        {
            return Settings();
        }

        errorMessage = "";
        this->fileName = fileName;

        switch(mode)
        {
        case Mode::Read:
        {
            SF_INFO sfInfo;
            sfInfo.format = 0;

            handle = sf_open(fileName.c_str(), SFM_READ, &sfInfo);

            if(handle == nullptr)
            {
                errorMessage = std::string("WAV::open - Mode::Read: ") + std::string(sf_strerror(handle));
                return Settings();
            }

            mode = Mode::Read;

            nChannels = sfInfo.channels;

            return Settings(sfInfo.samplerate, sfInfo.frames, sfInfo.channels);
        }

        case Mode::Write:
        {
            if(!writeSettings.isValid())
            {
                errorMessage = std::string("WAV::open - Mode::Write: Invalid settings.");
                return Settings();
            }

            SF_INFO sfInfo;
            sfInfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
            sfInfo.samplerate = writeSettings.getSampleRate();
            sfInfo.channels = writeSettings.getChannels();

            handle = sf_open(fileName.c_str(), SFM_WRITE, &sfInfo);

            if(handle == nullptr)
            {
                errorMessage = std::string("WAV::open - Mode::Write: ") + std::string(sf_strerror(handle));
                return Settings();
            }

            mode = Mode::Write;

            nChannels = sfInfo.channels;

            return Settings(sfInfo.samplerate, 0, sfInfo.channels);
        }

        default:
            errorMessage = std::string("WAV::open - Invalid mode.");
            return Settings();
        }
    }

    void close() override
    {
        if(mode != Mode::Invalid)
        {
            sf_close(handle);

            handle = nullptr;

            mode = Mode::Invalid;
        }
    }

    void reset() override
    {
        sf_seek(handle, 0, SEEK_SET);
    }

    const std::vector<std::vector<double>> read(size_t frames) override
    {
        size_t nValues = frames * nChannels;
        std::vector<float> input(nValues);

        size_t nValuesRead = sf_read_float(handle, input.data(), nValues);
        size_t nFramesRead = nValuesRead / nChannels;

        if(nFramesRead == 0)
        {
            errorMessage = "WAV::read: No more data to be read";
            return std::vector<std::vector<double>>();
        }

        std::vector<std::vector<double>> output;
        output.reserve(nChannels);

        for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
        {
            std::vector<double> channel;
            channel.reserve(nFramesRead);

            for(size_t iFrame = 0; iFrame < nFramesRead; ++iFrame)
            {
                size_t index = iFrame * nChannels + iChannel;
                channel.push_back(input[index]);
            }

            output.push_back(channel);
        }

        return output;
    }

    bool write(const std::vector<std::vector<double>>& input) override
    {
        size_t nInputChannels = input.size();
        size_t nInputFrames = input[0].size();

        size_t nValues = nInputFrames * nInputChannels;

        float output[nValues];

        if(input.size() != nChannels)
        {
            errorMessage = "WAV::write: Number of channels doesn't match.";
            return false;
        }

        for(size_t iChannel = 0; iChannel < nInputChannels; ++iChannel)
        {
            if(input[iChannel].size() != nInputFrames)
            {
                errorMessage = "WAV::write: Number of frames doesn't match.";
                return false;
            }

            for(size_t iFrame = 0; iFrame < nInputFrames; ++iFrame)
            {
                size_t index = iFrame * nChannels + iChannel;
                output[index] = input[iChannel][iFrame];
            }
        }

        size_t nValuesWritten = sf_write_float(handle, output, nValues);

        return nValuesWritten == nValues;
    }

private:
    SNDFILE* handle = nullptr;
};

}
