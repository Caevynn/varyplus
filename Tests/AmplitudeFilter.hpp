#pragma once

#include "Utils/ImpulseResponse.hpp"
#include "Utils/TransferFunction.hpp"

class AmplitudeFilter : public TransferFunction
{
    using Signal = std::vector<double>;

public:
    AmplitudeFilter(size_t blockSize, size_t nChannels, unsigned sampleRate, double amplitude)
        : TransferFunction(createImpulseResponse(nChannels, amplitude), blockSize, sampleRate, "Amplitude " + std::to_string(amplitude))
    {}

private:
    ImpulseResponse createImpulseResponse(size_t nChannels, double amplitude)
    {
        Signal signal(1, amplitude);

        ImpulseResponse impulseResponse;

        for (size_t iChannel = 0; iChannel < nChannels; ++iChannel)
        {
            impulseResponse.addChannel(signal);
        }

        return impulseResponse;
    }
};
