#pragma once

#include "Utils/FilterBase.hpp"
#include "Utils/LogSpectrum.hpp"

class BypassFilter : public FilterBase
{
    using Signal = std::vector<double>;

public:
    BypassFilter(size_t nChannels) : FilterBase("Bypass"), nChannels(nChannels) {}

    void reset() override {}

    void run(const Signal& input, std::vector<Signal>& output) override
    {
        for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
        {
            const auto& in = input;
            auto& out = output[iChannel];

            std::copy(in.begin(), in.end(), out.begin());
        }
    }

    void run(const std::vector<Signal>& input, std::vector<Signal>& output) override
    {
        for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
        {
            const auto& in = input[iChannel];
            auto& out = output[iChannel];

            std::copy(in.begin(), in.end(), out.begin());
        }
    }

    const LogSpectrum& getLogSpectrum() const override
    {
        static LogSpectrum emptyLogSpectrum;

        return emptyLogSpectrum;
    }

private:
    size_t nChannels;
};
