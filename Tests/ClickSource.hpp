#pragma once

#include <string>
#include <cmath>
#include <cassert>

#include "Utils/SourceBase.hpp"
#include "Utils/Sinc.hpp"

class ClickSource : public SourceBase
{
    using Signal = std::vector<double>;

public:
    ClickSource(unsigned sampleRate, double frequency, double amplitude)
        : SourceBase("Click " + std::to_string(frequency) + " Hz"),
          amplitude(amplitude),
          loopLength(sampleRate / frequency),
          click(Sinc::create(sampleRate, SincFrequency, 4))
    {}

    void reset() override
    {
        position = 0;
    }

    void read(size_t nSamples, Signal& output) override
    {
        assert(output.size() == nSamples);

        for(size_t iSample = 0; iSample < nSamples; ++iSample)
        {
            if(position < click.size())
            {
                output[iSample] = amplitude * click[position];
            }
            else
            {
                output[iSample] = 0;
            }

            position = (position + 1) % loopLength;
        }
    }

private:
    static constexpr double SincFrequency = 2000;

    double amplitude;
    size_t loopLength;
    size_t position{0};

    Signal click;
};
