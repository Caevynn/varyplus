#include "Copier.hpp"

#include "SoundFilePlus/Handler.hpp"

#include <iostream>

Copier::Copier()
{

}

bool Copier::copy(const std::string &sourceFileName, const std::string &destinationFileName)
{
    SoundFilePlus::Handler input, output;

    const auto inSettings = input.open(
                sourceFileName,
                SoundFilePlus::Mode::Read);

    if(!inSettings.isValid())
    {
        std::cout << "Failed to open source file: " << sourceFileName << std::endl;
        return false;
    }

    std::cout << inSettings.getSampleRate() << ", " << inSettings.getFrames() << ", " << inSettings.getChannels() << std::endl;

    const auto outSettings = output.open(
                destinationFileName,
                SoundFilePlus::Mode::Write,
                inSettings);

    if(!outSettings.isValid())
    {
        std::cout << "Failed to open destination file: " << destinationFileName << std::endl;
        return false;
    }

    size_t chunkSize = 512;

    for(;;)
    {
        auto chunk = input.read(chunkSize);

        if(chunk.size() == 0)
        {
            std::cout << "Reading failed: " << input.getErrorMessage() << std::endl;
            break;
        }

        if(!output.write(chunk))
        {
            std::cout << "Writing failed: " << output.getErrorMessage() << std::endl;
            return false;
        }
    }

    input.close();
    output.close();

    return true;
}
