#pragma once

#include <string>

class Copier
{
public:
    Copier();

    bool copy(const std::string& sourceFileName, const std::string& destinationFileName);
};
