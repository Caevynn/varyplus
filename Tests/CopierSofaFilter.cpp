#include "CopierSofaFilter.hpp"

#include "SoundFilePlus/Handler.hpp"
#include "Sofa/Filter.hpp"
#include "Sofa/File.hpp"

#include <iostream>

CopierSofaFilter::CopierSofaFilter(const std::string& filename) :
    filter(ConvolutionFilter::make<Sofa::Filter>(Sofa::File(filename), BLOCK_SIZE, std::make_shared<Sofa::Position>(0, 0, 0)))
{}

bool CopierSofaFilter::copy(const std::string &sourceFileName, const std::string &destinationFileName)
{
    SoundFilePlus::Handler input, output;

    const auto inSettings = input.open(
                sourceFileName,
                SoundFilePlus::Mode::Read);

    if(!inSettings.isValid())
    {
        std::cout << "Failed to open source file: " << sourceFileName << std::endl;
        return false;
    }

    std::cout << inSettings.getSampleRate() << ", " << inSettings.getFrames() << ", " << inSettings.getChannels() << std::endl;

    const auto outSettings = output.open(
                destinationFileName,
                SoundFilePlus::Mode::Write,
                inSettings);

    if(!outSettings.isValid())
    {
        std::cout << "Failed to open destination file: " << destinationFileName << std::endl;
        return false;
    }

    for(;;)
    {
        auto data = input.read(BLOCK_SIZE);

        if(data[0].size() != BLOCK_SIZE)
        {
            std::cout << "Reading failed: " << input.getErrorMessage() << std::endl;
            break;
        }

        auto outputData = data;
        filter.run(data, outputData);

        if(!output.write(outputData))
        {
            std::cout << "Writing failed: " << output.getErrorMessage() << std::endl;
            return false;
        }
    }

    input.close();
    output.close();

    return true;
}
