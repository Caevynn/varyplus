#pragma once

#include <string>

#include "Utils/ConvolutionFilter.hpp"

class CopierSofaFilter
{
public:
    CopierSofaFilter(const std::string& filename);

    bool copy(const std::string& sourceFileName, const std::string& destinationFileName);

private:
    static constexpr size_t BLOCK_SIZE = 2048;

    ConvolutionFilter filter;
};
