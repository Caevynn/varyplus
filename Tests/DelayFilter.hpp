#pragma once

#include "Utils/ImpulseResponse.hpp"
#include "Utils/TransferFunction.hpp"

class DelayFilter : public TransferFunction
{
    using Signal = std::vector<double>;

public:
    DelayFilter(size_t blockSize, size_t nChannels, double sampleRate, double time, double amplitude)
        : TransferFunction(createImpulseResponse(nChannels, sampleRate, time, amplitude), blockSize, sampleRate, "Delay " + std::to_string(time) + " s")
    {}

private:
    ImpulseResponse createImpulseResponse(size_t nChannels, double sampleRate, double time, double amplitude)
    {
        size_t delayIndex = time * sampleRate;
        size_t signalLength = delayIndex + 1;

        Signal signal(signalLength, 0);
        signal[0] = 1.0;
        signal[delayIndex] = amplitude;

        ImpulseResponse impulseResponse;

        for(size_t iChannel = 0; iChannel < nChannels; ++ iChannel)
        {
            impulseResponse.addChannel(signal);
        }

        return impulseResponse;
    }
};
