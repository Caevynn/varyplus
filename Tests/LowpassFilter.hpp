#pragma once

#include "Utils/ImpulseResponse.hpp"
#include "Utils/TransferFunction.hpp"
#include "Utils/Sinc.hpp"

class LowpassFilter : public TransferFunction
{
    using Signal = std::vector<double>;

public:
    LowpassFilter(size_t blockSize, size_t nChannels, double sampleRate, double frequency)
        : TransferFunction(createImpulseResponse(nChannels, sampleRate, frequency), blockSize, sampleRate, "Lowpass " + std::to_string(frequency) + " Hz")
    {}

private:
    ImpulseResponse createImpulseResponse(size_t nChannels, double sampleRate, double frequency)
    {
        Signal signal = Sinc::create(sampleRate, frequency, 32);

        double sum = 0;

        for(const auto& value : signal)
        {
            sum += value;
        }

        for(auto& value : signal)
        {
            value /= sum;
        }

        ImpulseResponse impulseResponse;

        for (size_t iChannel = 0; iChannel < nChannels; ++iChannel)
        {
            impulseResponse.addChannel(signal);
        }

        return impulseResponse;
    }
};
