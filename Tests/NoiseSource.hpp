#pragma once

#include <string>
#include <random>
#include <algorithm>
#include <cassert>

#include "Utils/SourceBase.hpp"

class NoiseSource : public SourceBase
{
    using Signal = std::vector<double>;

public:
    NoiseSource(double amplitude)
        : SourceBase("Noise"),
          randomDevice(),
          randomGenerator(randomDevice()),
          normalDistribution(0.0, amplitude)
    {}

    void reset() override {}

    void read(size_t nSamples, Signal& output) override
    {
        assert(output.size() == nSamples);

        for(size_t iSample = 0; iSample < nSamples; ++iSample)
        {
            output[iSample] = std::clamp(normalDistribution(randomGenerator), -1.0, 1.0);
        }
    }

private:
    std::random_device randomDevice;
    std::mt19937 randomGenerator;
    std::normal_distribution<double> normalDistribution;
};
