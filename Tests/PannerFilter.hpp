#pragma once

#include <memory>

#include "Utils/TransferFunctionBase.hpp"
#include "Utils/TransferFunction.hpp"
#include "Utils/ImpulseResponse.hpp"
#include "Sofa/Position.hpp"

class PannerFilter : public TransferFunctionBase
{
    using Signal = std::vector<double>;
    using Spectrum = std::vector<std::complex<double>>;

public:
    PannerFilter(size_t blockSize, unsigned sampleRate, std::shared_ptr<Sofa::Position> position)
        : TransferFunctionBase(blockSize, "Panner"),
          position(position)
    {
        size_t positionsPerSide = 18;
        size_t nPositions = 2 * positionsPerSide + 1;

        for(size_t iPosition = 0; iPosition < nPositions; ++iPosition)
        {
            double angle = (static_cast<double>(iPosition) - positionsPerSide) / positionsPerSide;

            double ratio = static_cast<double>(iPosition) / (nPositions - 1);
            double leftAmplitude = std::sqrt(1.0 - ratio) / std::sqrt(0.5);
            double rightAmplitude = std::sqrt(ratio) / std::sqrt(0.5);

            ImpulseResponse impulseResponse;
            impulseResponse.addChannel(Signal({leftAmplitude}));
            impulseResponse.addChannel(Signal({rightAmplitude}));

            filterList.push_back(TransferFunction(impulseResponse, blockSize, sampleRate, ""));
            positionList.push_back(Sofa::Position(angle * 90, 0, 0));
        }
    }

    void apply(const OverlapAddBuffer& input, std::vector<Spectrum>& output) const override
    {
        return filterList[getPositionIndex(*position)].apply(input, output);
    }

    size_t getNumPartitions() const override { return filterList.front().getNumPartitions(); }
    size_t getSpectrumSize() const override { return filterList.front().getSpectrumSize(); }
    size_t getNumChannels() const override { return filterList.front().getNumChannels(); }

    const LogSpectrum& getLogSpectrum() const override
    {
        return filterList[getPositionIndex(*position)].getLogSpectrum();
    }

private:
    std::vector<TransferFunction> filterList;
    std::vector<Sofa::Position> positionList;

    std::shared_ptr<Sofa::Position> position;

    size_t getPositionIndex(const Sofa::Position& position) const
    {
        double minDistance = positionList[0].distance(position);
        size_t nearestPositionIndex = 0;

        for(size_t i = 1; i < positionList.size(); ++i)
        {
            double currentDistance = positionList[i].distance(position);

            if(currentDistance < minDistance)
            {
                minDistance = currentDistance;
                nearestPositionIndex = i;
            }
        }

        return nearestPositionIndex;
    }
};
