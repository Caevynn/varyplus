#include "Player.hpp"

#include "PortAudioPlus/DeviceList.hpp"

Player::Player()
{

}

bool Player::play(const std::string &fileName)
{
    auto settings = fileHandler.open(fileName, SoundFilePlus::Mode::Read);

    if(!settings.isValid())
    {
        return false;
    }

    PortAudioPlus::Stream stream;
    stream.mode = PortAudioPlus::Mode::Play;
    stream.device = PortAudioPlus::DeviceList::getDefaultOutputDevice();
    stream.sampleRate = settings.getSampleRate();
    stream.numChannels = settings.getChannels();
    stream.framesPerBuffer = 256;

    if(!audioInterface.start(stream, *this))
    {
        fileHandler.close();
        return false;
    }

    return true;
}

bool Player::handleOutput()
{
    size_t nFrames = buffer.output.getNumFrames();
    size_t nChannels = buffer.output.getNumChannels();

    std::vector<std::vector<double>> data = fileHandler.read(nFrames);

    if(data.size() != nChannels)
    {
        return false;
    }

    for(const auto& channel : data)
    {
        if(channel.size() != nFrames)
        {
            return false;
        }
    }

    buffer.output.set(data);

    return true;
}
