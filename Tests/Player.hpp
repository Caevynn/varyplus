#pragma once

#include "SoundFilePlus/Handler.hpp"
#include "PortAudioPlus/Interface.hpp"

class Player : public PortAudioPlus::CallbackHandler
{
public:
    Player();

    bool play(const std::string& fileName);

private:
    SoundFilePlus::Handler fileHandler;

    // IMPORTANT: Construct last to be destructed first to stop
    //            audio thread before freeing resources (e.g.
    //            OverlapAdd)
    PortAudioPlus::Interface audioInterface;

    bool handleOutput() override;
};
