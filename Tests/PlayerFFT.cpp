#include "PlayerFFT.hpp"

#include <iostream>

#include "PortAudioPlus/DeviceList.hpp"

PlayerFFT::PlayerFFT() :
    rfft(CHUNK_SIZE)
{

}

bool PlayerFFT::play(const std::string &fileName)
{
    auto settings = fileHandler.open(fileName, SoundFilePlus::Mode::Read);

    if(!settings.isValid())
    {
        return false;
    }

    PortAudioPlus::Stream stream;
    stream.mode = PortAudioPlus::Mode::Play;
    stream.device = PortAudioPlus::DeviceList::getDefaultOutputDevice();
    stream.sampleRate = settings.getSampleRate();
    stream.numChannels = settings.getChannels();
    stream.framesPerBuffer = CHUNK_SIZE;

    if(!audioInterface.start(stream, *this))
    {
        fileHandler.close();
        return false;
    }

    return true;
}

bool PlayerFFT::handleOutput()
{
    size_t nFrames = buffer.output.getNumFrames();
    size_t nChannels = buffer.output.getNumChannels();

    std::vector<std::vector<double>> data = fileHandler.read(nFrames);

    if(data.size() != nChannels)
    {
        return false;
    }

    for(const auto& channel : data)
    {
        if(channel.size() != nFrames)
        {
            return false;
        }
    }

    for(auto& channel : data)
    {
        std::vector<std::complex<double>> spectrum(channel.size() / 2 + 1);
        rfft.forward(channel, spectrum);
        rfft.inverse(spectrum, channel);
    }

    buffer.output.set(data);

    return true;
}
