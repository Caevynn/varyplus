#pragma once

#include "SoundFilePlus/Handler.hpp"
#include "PortAudioPlus/Interface.hpp"

#include "RealFFT/RealFFT.hpp"

class PlayerFFT : public PortAudioPlus::CallbackHandler
{
public:
    PlayerFFT();

    bool play(const std::string& fileName);

private:
    static const int CHUNK_SIZE = 256;

    RealFFT rfft;

    SoundFilePlus::Handler fileHandler;

    // IMPORTANT: Construct last to be destructed first to stop
    //            audio thread before freeing resources (e.g.
    //            OverlapAdd)
    PortAudioPlus::Interface audioInterface;

    bool handleOutput() override;
};
