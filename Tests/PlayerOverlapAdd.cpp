#include "PlayerOverlapAdd.hpp"

#include "PortAudioPlus/DeviceList.hpp"

PlayerOverlapAdd::PlayerOverlapAdd()
{

}

bool PlayerOverlapAdd::play(const std::string &fileName)
{
    auto settings = fileHandler.open(fileName, SoundFilePlus::Mode::Read);

    if(!settings.isValid())
    {
        return false;
    }

    overlapAdd = OverlapAdd(BLOCK_SIZE, settings.getChannels());

    PortAudioPlus::Stream stream;
    stream.mode = PortAudioPlus::Mode::Play;
    stream.device = PortAudioPlus::DeviceList::getDefaultOutputDevice();
    stream.sampleRate = settings.getSampleRate();
    stream.numChannels = settings.getChannels();
    stream.framesPerBuffer = BLOCK_SIZE;

    if(!audioInterface.start(stream, *this))
    {
        fileHandler.close();
        return false;
    }

    return true;
}

bool PlayerOverlapAdd::handleOutput()
{
    size_t nFrames = buffer.output.getNumFrames();
    size_t nChannels = buffer.output.getNumChannels();

    std::vector<std::vector<double>> data = fileHandler.read(nFrames);

    if(data.size() != nChannels)
    {
        return false;
    }

    for(const auto& channel : data)
    {
        if(channel.size() != nFrames)
        {
            return false;
        }
    }

    std::vector<std::vector<std::complex<double>>> spectrums(data.size(), std::vector<std::complex<double>>(2 * nFrames + 1));
    overlapAdd.input(data, spectrums);
    overlapAdd.output(spectrums, data);
    buffer.output.set(data);

    return true;
}
