#pragma once

#include "PortAudioPlus/Interface.hpp"
#include "SoundFilePlus/Handler.hpp"

#include "Utils/OverlapAdd.hpp"

class PlayerOverlapAdd : public PortAudioPlus::CallbackHandler
{
public:
    PlayerOverlapAdd();

    bool play(const std::string& fileName);

private:
    static const int BLOCK_SIZE = 256;

    OverlapAdd overlapAdd;

    SoundFilePlus::Handler fileHandler;

    // IMPORTANT: Construct last to be destructed first to stop
    //            audio thread before freeing resources (e.g.
    //            OverlapAdd)
    PortAudioPlus::Interface audioInterface;

    bool handleOutput() override;
};
