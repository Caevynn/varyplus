#include "PlayerSofaFilter.hpp"

#include <memory>

#include "Sofa/File.hpp"
#include "Sofa/Filter.hpp"
#include "PortAudioPlus/DeviceList.hpp"

#include <iostream>

PlayerSofaFilter::PlayerSofaFilter()
{}

bool PlayerSofaFilter::play(const std::string &audioFile, const std::string& sofaFile)
{
    const Sofa::File file(sofaFile);

    if(!file.isValid())
    {
        std::cout << "Sofa file not valid" << std::endl;
        return false;
    }

    filter = ConvolutionFilter::makeUnique<Sofa::Filter>(file, BLOCK_SIZE, std::make_shared<Sofa::Position>(0, 0, 0));

    auto settings = fileHandler.open(audioFile, SoundFilePlus::Mode::Read);

    if(!settings.isValid())
    {
        return false;
    }

    PortAudioPlus::Stream stream;
    stream.mode = PortAudioPlus::Mode::Play;
    stream.device = PortAudioPlus::DeviceList::getDefaultOutputDevice();
    stream.sampleRate = settings.getSampleRate();
    stream.numChannels = settings.getChannels();
    stream.framesPerBuffer = BLOCK_SIZE;

    if(!audioInterface.start(stream, *this))
    {
        fileHandler.close();
        return false;
    }

    return true;
}

bool PlayerSofaFilter::handleOutput()
{
    size_t nFrames = buffer.output.getNumFrames();

    Signal data = fileHandler.readMono(nFrames);

    if(data.size() != nFrames)
    {
        return false;
    }

    std::vector<Signal> outputData(2, Signal(data.size()));
    filter->run(data, outputData);
    buffer.output.set(outputData);

    return true;
}

void PlayerSofaFilter::handleFinished()
{
    auto error = getErrorMessage();

    if(!error.empty())
    {
        std::cout << error << std::endl;
    }
}
