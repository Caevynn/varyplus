#pragma once

#include "PortAudioPlus/Interface.hpp"
#include "SoundFilePlus/Handler.hpp"
#include "Utils/ConvolutionFilter.hpp"

class PlayerSofaFilter : public PortAudioPlus::CallbackHandler
{
    using Signal = std::vector<double>;

public:
    PlayerSofaFilter();

    bool play(const std::string& audioFile, const std::string& sofaFile);

private:
    static constexpr size_t BLOCK_SIZE = 1024;

    std::unique_ptr<ConvolutionFilter> filter;

    SoundFilePlus::Handler fileHandler;

    // IMPORTANT: Construct last to be destructed first to stop
    //            audio thread before freeing resources (e.g.
    //            OverlapAdd)
    PortAudioPlus::Interface audioInterface;

    bool handleOutput() override;
    void handleFinished() override;
};
