#include "PlayerTestFilter.hpp"

#include "PortAudioPlus/DeviceList.hpp"

#include "Tests/AmplitudeFilter.hpp"
#include "Tests/LowpassFilter.hpp"
#include "Tests/DelayFilter.hpp"
#include "Utils/ConvolutionFilter.hpp"

#include <iostream>

PlayerTestFilter::PlayerTestFilter()
{
    filters.push_back(ConvolutionFilter::make<AmplitudeFilter>(BlockSize, NChannels, SampleRate, 0.1));
    filters.push_back(ConvolutionFilter::make<LowpassFilter>(BlockSize, NChannels, SampleRate, 1000));
    filters.push_back(ConvolutionFilter::make<DelayFilter>(BlockSize, NChannels, SampleRate, 0.5, 0.5));
}

bool PlayerTestFilter::play(const std::string &fileName, Type type)
{
    switch(type)
    {
    case Type::Amplitude:
        activeFilter = 0;
        break;

    case Type::Lowpass:
        activeFilter = 1;
        break;

    case Type::Delay:
        activeFilter = 2;
        break;

    default:
        std::cout << "PlayerTestFilter::play - invalid type." << std::endl;
        return false;
    }

    auto settings = fileHandler.open(fileName, SoundFilePlus::Mode::Read);

    if(!settings.isValid())
    {
        return false;
    }

    if(settings.getSampleRate() != SampleRate)
    {
        std::cout << "PlayerTestFilter::play - sample rates don't match (Filter: " << SampleRate << ", File: " << settings.getSampleRate() << ")" << std::endl;
        return false;
    }

    PortAudioPlus::Stream stream;
    stream.mode = PortAudioPlus::Mode::Play;
    stream.device = PortAudioPlus::DeviceList::getDefaultOutputDevice();
    stream.sampleRate = settings.getSampleRate();
    stream.numChannels = settings.getChannels();
    stream.framesPerBuffer = BlockSize;

    if(!audioInterface.start(stream, *this))
    {
        fileHandler.close();
        return false;
    }

    return true;
}

bool PlayerTestFilter::handleOutput()
{
    size_t nFrames = buffer.output.getNumFrames();
    size_t nChannels = buffer.output.getNumChannels();

    std::vector<Signal> data = fileHandler.read(nFrames);

    if(data.size() != nChannels)
    {
        return false;
    }

    for(const auto& channel : data)
    {
        if(channel.size() != nFrames)
        {
            return false;
        }
    }

    auto outputData = data;
    filters.at(activeFilter).run(data, outputData);
    buffer.output.set(outputData);

    return true;
}
