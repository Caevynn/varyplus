#pragma once

#include "PortAudioPlus/Interface.hpp"
#include "SoundFilePlus/Handler.hpp"

class ConvolutionFilter;

class PlayerTestFilter : public PortAudioPlus::CallbackHandler
{
    using Signal = std::vector<double>;

public:
    enum class Type
    {
        Amplitude,
        Lowpass,
        Delay
    };

    PlayerTestFilter();

    bool play(const std::string& fileName, Type type);

private:
    static const int BlockSize = 256;
    static const int SampleRate = 44100;
    static const int NChannels = 2;

    Type type;

    size_t activeFilter = 0;
    std::vector<ConvolutionFilter> filters;

    SoundFilePlus::Handler fileHandler;

    // IMPORTANT: Construct last to be destructed first to stop
    //            audio thread before freeing resources (e.g.
    //            OverlapAdd)
    PortAudioPlus::Interface audioInterface;

    bool handleOutput() override;
};
