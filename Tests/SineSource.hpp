#pragma once

#include <string>
#include <cmath>
#include <cassert>

#include "Utils/SourceBase.hpp"

class SineSource : public SourceBase
{
    using Signal = std::vector<double>;

public:
    SineSource(unsigned sampleRate, double frequency, double amplitude)
        : SourceBase("Sine " + std::to_string(frequency) + " Hz"),
          sampleRate(sampleRate),
          frequency(frequency),
          amplitude(amplitude)
    {}

    void reset() override
    {
        position = 0;
    }

    void read(size_t nSamples, Signal& output) override
    {
        assert(output.size() == nSamples);

        for(size_t iSample = 0; iSample < nSamples; ++iSample)
        {
            output[iSample] = amplitude * std::sin(2 * M_PI * position);

            position += frequency / sampleRate;
        }
    }

private:
    unsigned sampleRate;
    double frequency;
    double amplitude;
    double position{0};
};
