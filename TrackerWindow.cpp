#include "TrackerWindow.hpp"

#include <QKeyEvent>

#include "CustomObjects/UDPTracker.hpp"
#include "CustomWidgets/Slider2D.hpp"

TrackerWindow::TrackerWindow(QWidget *parent)
    : QMainWindow(parent)
{
    udpTracker = new UDPTracker(5556, this);

    positionSlider = new Slider2D(true);
    positionSlider->setContentsMargins(15, 15, 15, 15);
    connect(positionSlider, &Slider2D::positionChanged, this, &TrackerWindow::positionChanged);

    setCentralWidget(positionSlider);
}

void TrackerWindow::positionChanged(const Sofa::Position& position)
{
    udpTracker->send(position, 5555);
}

void TrackerWindow::keyPressEvent(QKeyEvent* event)
{
    // Forward events to parent to access shortcuts
    parent()->event(static_cast<QEvent*>(event));
}
