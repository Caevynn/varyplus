#pragma once

#include <QMainWindow>

class UDPTracker;
class Slider2D;

namespace Sofa { class Position; }

class TrackerWindow : public QMainWindow
{
public:
    TrackerWindow(QWidget *parent = nullptr);

private slots:
    void positionChanged(const Sofa::Position& position);

private:
    UDPTracker* udpTracker;
    Slider2D* positionSlider;

    void keyPressEvent(QKeyEvent* event) override;
};
