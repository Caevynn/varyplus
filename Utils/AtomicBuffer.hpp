#pragma once

#include <atomic>
#include <vector>

template<typename T, size_t Size = 8>
class AtomicBuffer
{
public:
    AtomicBuffer(const T& initialElement = T())
        : data(Size, initialElement), latestPtr(0)
    {
        static_assert(std::atomic<size_t>::is_always_lock_free);
    }

    AtomicBuffer(const AtomicBuffer<T>& other)
        : data(other.data), latestPtr(other.latestPtr.load())
    {}

    AtomicBuffer<T>& operator=(const AtomicBuffer<T>& other)
    {
        data = other.data;
        latestPtr.store(other.latestPtr.load());
        return *this;
    }

    void store(const T& element)
    {
        size_t currentLatest = latestPtr.load();
        size_t nextLatest = (currentLatest + 1) % Size;

        data[nextLatest] = element;
        latestPtr.store(nextLatest);
    }

    void load(T& element)
    {
        element = data[latestPtr.load()];
    }

private:
    std::vector<T> data;

    std::atomic<size_t> latestPtr;
};
