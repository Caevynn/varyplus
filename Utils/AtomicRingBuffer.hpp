#pragma once

#include <atomic>
#include <optional>
#include <vector>

template<typename T, size_t Size = 8>
class AtomicRingBuffer
{
public:
    AtomicRingBuffer(const T& initialElement = T())
        : data(Size, initialElement), writePtr(0), readPtr(0)
    {
        static_assert(std::atomic<size_t>::is_always_lock_free);
    }

    AtomicRingBuffer(const AtomicRingBuffer<T>& other)
        : data(other.data), writePtr(other.writePtr.load()), readPtr(other.readPtr.load())
    {}

    AtomicRingBuffer<T>& operator=(const AtomicRingBuffer<T>& other)
    {
        data = other.data;
        writePtr.store(other.writePtr.load());
        readPtr.store(other.readPtr.load());
    }

    bool push(const T& element)
    {
        size_t currentRead = readPtr.load();
        size_t currentWrite = writePtr.load();
        size_t nextWrite = (currentWrite + 1) % Size;

        if(nextWrite == currentRead)
        {
            return false;
        }

        data[currentWrite] = element;
        writePtr.store(nextWrite);

        return true;
    }

    bool pop(T& element)
    {
        size_t currentRead = readPtr.load();
        size_t currentWrite = writePtr.load();
        size_t nextRead = (currentRead + 1) % Size;

        if(currentRead == currentWrite)
        {
            return false;
        }

        element = data[currentRead];
        readPtr.store(nextRead);

        return true;
    }

private:
    std::vector<T> data;

    std::atomic<size_t> writePtr;
    std::atomic<size_t> readPtr;
};
