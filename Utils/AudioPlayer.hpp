#pragma once

#include <memory>
#include <cassert>

#include "SourceBase.hpp"
#include "PlaybackState.hpp"

class AudioPlayer
{
    using Signal = std::vector<double>;

public:
    template<typename T, typename ...Args>
    static std::unique_ptr<AudioPlayer> makeUnique(size_t blockSize, Args ...args)
    {
        return std::make_unique<AudioPlayer>(blockSize, std::make_unique<T>(args...));
    }

    AudioPlayer(size_t blockSize, std::unique_ptr<SourceBase> source)
        : blockSize(blockSize),
          tmpSignal(blockSize),
          currentState(source->getLength()),
          nextState(source->getLength()),
          source(std::move(source))
    {}

    void read(std::vector<Signal>& output)
    {
        if(currentState.isRunning() && currentState.getOutput() < output.size())
        {
            Signal& outputSignal = output[currentState.getOutput()];

            assert(outputSignal.size() == blockSize);

            source->read(currentState.getPosition().getPosition(), blockSize, tmpSignal);

            for(size_t iSample = 0; iSample < blockSize; ++iSample)
            {
                double factor = static_cast<double>(blockSize - iSample) / blockSize;
                outputSignal[iSample] += tmpSignal[iSample] * factor * currentState.getVolume();
            }
        }

        if(nextState.isRunning() && nextState.getOutput() < output.size())
        {
            Signal& outputSignal = output[nextState.getOutput()];

            assert(outputSignal.size() == blockSize);

            if(!currentState.isRunning() || currentState.getPosition().getPosition() != nextState.getPosition().getPosition())
            {
                // Don't read new data if same as current state (important for generic sources)
                source->read(nextState.getPosition().getPosition(), blockSize, tmpSignal);
            }

            if(!currentState.isRunning() && nextState.getPosition().getPosition() == 0)
            {
                // Don't fade in if playback starts at beginning
                for(size_t iSample = 0; iSample < blockSize; ++iSample)
                {
                    outputSignal[iSample] += tmpSignal[iSample] * nextState.getVolume();
                }
            }
            else
            {
                for(size_t iSample = 0; iSample < blockSize; ++iSample)
                {
                    double factor =  static_cast<double>(iSample) / blockSize;
                    outputSignal[iSample] += tmpSignal[iSample] * factor * nextState.getVolume();
                }
            }
        }

        nextState.increasePositionIfRunning(blockSize);
        currentState = nextState;
    }

    void setCommand(const PlaybackCommand& command)
    {
        nextState.apply(command);
    }

    const PlaybackState& getState() const
    {
        return currentState;
    }

    const std::string& getName() const
    {
        return source->getName();
    }

private:
    size_t blockSize;

    Signal tmpSignal;

    PlaybackState currentState, nextState;

    std::unique_ptr<SourceBase> source;
};
