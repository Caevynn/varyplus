#pragma once

#include <memory>

#include "Utils/FilterBase.hpp"
#include "Utils/TransferFunctionBase.hpp"
#include "Utils/OverlapAdd.hpp"
#include "Utils/OverlapAddBuffer.hpp"

class ConvolutionFilter : public FilterBase
{
    using Signal = std::vector<double>;
    using Spectrum = std::vector<std::complex<double>>;

public:
    template<typename T, typename ...Args>
    static ConvolutionFilter make(Args ...args)
    {
        return ConvolutionFilter(std::make_unique<T>(args...));
    }

    template<typename T, typename ...Args>
    static std::unique_ptr<ConvolutionFilter> makeUnique(Args ...args)
    {
        return std::make_unique<ConvolutionFilter>(std::make_unique<T>(args...));
    }

    ConvolutionFilter(std::unique_ptr<TransferFunctionBase> transferFunction)
        : FilterBase(transferFunction->getName()),
          spectrumBuffer(transferFunction->getSpectrumSize(), transferFunction->getNumPartitions(), transferFunction->getNumChannels()),
          overlapAdd(transferFunction->getBlockSize(), transferFunction->getNumChannels()),
          tmpSpectrums(transferFunction->getNumChannels(), Spectrum(2 * transferFunction->getBlockSize() + 1)),
          transferFunction(std::move(transferFunction))
    {}

    void reset() override
    {
        spectrumBuffer.reset();
        overlapAdd.reset();
    }

    void run(const Signal& input, std::vector<Signal>& output) override
    {
        auto& tmpSpectrum = tmpSpectrums[0];

        overlapAdd.input(input, tmpSpectrum);
        spectrumBuffer.push_back(tmpSpectrum);
        transferFunction->apply(spectrumBuffer, tmpSpectrums);
        overlapAdd.output(tmpSpectrums, output);
    }

    void run(const std::vector<Signal>& input, std::vector<Signal>& output) override
    {
        overlapAdd.input(input, tmpSpectrums);
        spectrumBuffer.push_back(tmpSpectrums);
        transferFunction->apply(spectrumBuffer, tmpSpectrums);
        overlapAdd.output(tmpSpectrums, output);
    }

    const LogSpectrum& getLogSpectrum() const override
    {
        return transferFunction->getLogSpectrum();
    }

private:
    OverlapAddBuffer spectrumBuffer;
    OverlapAdd overlapAdd;

    std::vector<Spectrum> tmpSpectrums;

    std::unique_ptr<TransferFunctionBase> transferFunction;
};
