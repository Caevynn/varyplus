#pragma once

#include <chrono>
#include <functional>
#include <map>
#include <iostream>

#define DURATION(name, func) \
{ \
    Duration::Timer macroTimer(name); \
    func; \
}

class Duration
{
public:
    using ClockT = std::chrono::steady_clock;
    using TimeT = std::chrono::microseconds;
    using TimePointT = std::chrono::time_point<ClockT>;
    using MeasurementT = std::pair<TimePointT, TimePointT>;

    template <class F, class ...Args,
              std::enable_if_t<!std::is_void<typename std::result_of<F&&(Args&&...)>::type>::value, bool> = true>
    static typename std::result_of<F&&(Args&&...)>::type
    Wrapper(const std::string& name, F&& func, Args&&... args)
    {
        MeasurementT measurement;
        measurement.first = ClockT::now();
        typename std::result_of<F&&(Args&&...)>::type result = std::invoke(std::forward<F>(func), std::forward<Args>(args)...);
        measurement.second = ClockT::now();

        setMeasurement(name, measurement);

        return result;
    }

    template <class F, class ...Args,
              std::enable_if_t<std::is_void<typename std::result_of<F&&(Args&&...)>::type>::value, bool> = true>
    static void
    Wrapper(const std::string& name, F&& func, Args&&... args)
    {
        MeasurementT measurement;
        measurement.first = ClockT::now();
        std::invoke(std::forward<F>(func), std::forward<Args>(args)...);
        measurement.second = ClockT::now();

        setMeasurement(name, measurement);
    }

    class Timer
    {
    public:
        Timer(const std::string& name)
            : name_(name)
        {
            start();
        }

        ~Timer()
        {
            stop();
        }

        void start()
        {
            if(!running_)
            {
                measurement_.first = ClockT::now();
            }

            running_ = true;
        }

        void stop()
        {
            if(running_)
            {
                measurement_.second = ClockT::now();
                setMeasurement(name_, measurement_);
            }

            running_ = false;
        }

    private:
        bool running_{false};
        std::string name_;
        MeasurementT measurement_;
    };

    class Measurement
    {
    public:
        void addMeasurement(const MeasurementT& meas)
        {
            TimeT cur = std::chrono::duration_cast<TimeT>(meas.second - meas.first);

            min = std::min(cur, min);
            max = std::max(cur, max);

            sum += cur;
            ++nMeasurements;
        }

        void print(const std::string& key) const
        {
            std::cout << key
                      << ": avg = " << sum.count() / nMeasurements << " us"
                      << ", min = " << min.count() << " us"
                      << ", max = " << max.count() << " us"
                      << ", total = " << sum.count() / 1000000.0 << " s"
                      << ", n = " << nMeasurements << std::endl;
        }

    private:
        TimeT min{TimeT::max()};
        TimeT max{TimeT::min()};
        TimeT sum{TimeT::zero()};
        size_t nMeasurements{0};
    };

    class LocalTimer
    {
    public:
        LocalTimer(const std::string& name)
            : name(name)
        {}

        void start()
        {
            currentMeasurement.first = ClockT::now();
        }

        void stop()
        {
            currentMeasurement.second = ClockT::now();
            accumulatedMeasurement.addMeasurement(currentMeasurement);
        }

        void print() const
        {
            accumulatedMeasurement.print(name);
        }

    private:
        std::string name;
        MeasurementT currentMeasurement;
        Measurement accumulatedMeasurement;
    };

    class LocalTimerGuard
    {
    public:
        LocalTimerGuard(LocalTimer& timer)
            : timerPtr(&timer)
        {
            timerPtr->start();
        }

        ~LocalTimerGuard()
        {
            timerPtr->stop();
        }

    private:
        LocalTimer* timerPtr;
    };

    static void print()
    {
        std::cout << std::endl;

        for(const auto& [key, value] : getInstance().measurements)
        {
            value.print(key);
        }
    }

    static void printNow()
    {
        TimeT time = std::chrono::duration_cast<TimeT>(ClockT::now() - getInstance().startTime);
        std::cout << time.count() << std::endl;
    }

    static void reset()
    {
        getInstance().measurements.clear();
    }

private:
    static Duration& getInstance()
    {
        static Duration instance;
        return instance;
    }

    static void setMeasurement(const std::string& name, const MeasurementT& measurement)
    {
        getInstance().measurements[name].addMeasurement(measurement);
    }

    std::map<std::string, Measurement> measurements;

    TimePointT startTime;

    // Make constructor private and delete move/copy
    Duration() { startTime = ClockT::now(); }
    Duration(const Duration&) = delete;
    Duration& operator=(const Duration&) = delete;
    Duration(Duration&&) = delete;
    Duration& operator=(Duration&&) = delete;
};
