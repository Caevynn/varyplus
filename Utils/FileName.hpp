#pragma once

#include <string>
#include <algorithm>

class FileName
{
public:
    static std::string removePath(const std::string& fileName)
    {
        size_t pos = fileName.find_last_of("/\\");

        if(pos != std::string::npos)
        {
            return fileName.substr(pos + 1);
        }
        else
        {
            return fileName;
        }
    }

    static std::string getAppendix(const std::string& fileName)
    {
        size_t pos = fileName.find_last_of(".");

        if(pos == std::string::npos)
        {
            return "";
        }

        std::string appendix = fileName.substr(pos + 1);

        std::transform(appendix.begin(), appendix.end(), appendix.begin(), [](unsigned char c)
        {
            return std::tolower(c);
        });

        return appendix;
    }
};
