#pragma once

#include <vector>
#include <string>

class LogSpectrum;

class FilterBase
{
    using Signal = std::vector<double>;

public:
    FilterBase(const std::string& name) : name(name) {}
    virtual ~FilterBase() = default;

    virtual void reset() {}

    virtual void run(const Signal& input, std::vector<Signal>& output) = 0;
    virtual void run(const std::vector<Signal>& input, std::vector<Signal>& output) = 0;

    virtual const LogSpectrum& getLogSpectrum() const = 0;

    const std::string& getName() const { return name; }

private:
    std::string name;
};
