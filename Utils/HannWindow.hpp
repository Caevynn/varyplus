#pragma once

#include <cmath>
#include <vector>
#include <algorithm>
#include <functional>
#include <cassert>

class HannWindow
{
public:
    HannWindow(size_t size)
    {
        window.reserve(size);

        for (size_t i = 0; i < size; ++i)
        {
            double position = static_cast<double>(i) / static_cast<double>(size);
            window.push_back(0.5 - 0.5 * std::cos(2 * M_PI * position));
        }
    }

    void apply(std::vector<double>& signal) const
    {
        assert(signal.size() == window.size());

        std::transform(window.begin(), window.end(), signal.begin(), signal.begin(), std::multiplies<double>());
    }

private:
    std::vector<double> window;
};
