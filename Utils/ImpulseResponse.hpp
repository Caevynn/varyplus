#pragma once

#include <vector>
#include <stdexcept>

class ImpulseResponse
{
    using Signal = std::vector<double>;

public:
    const Signal& getChannel(size_t iChannel) const
    {
        return channels.at(iChannel);
    }

    void addChannel(const Signal& channel)
    {
        if(nChannels() != 0 && channel.size() != nSamples())
        {
            throw std::runtime_error("ImpulseResponse: size of channel doesn't match");
        }

        channels.push_back(channel);
    }

    size_t nChannels() const { return channels.size(); }
    size_t nSamples() const { return channels.at(0).size(); }

private:
    std::vector<Signal> channels;
};
