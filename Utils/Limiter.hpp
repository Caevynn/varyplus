#pragma once

#include <vector>
#include <cmath>
#include <cassert>

class Limiter
{
    using Signal = std::vector<double>;

public:
    Limiter(unsigned sampleRate)
        : decayFactor(std::pow(0.1, 1.0 / sampleRate))
    {}

    void setNumChannels(size_t nChannels)
    {
        peak.clear();
        peak.resize(nChannels, 0);
    }

    void run(Signal& input)
    {
        assert(peak.size() == 1);

        size_t nSamples = input.size();

        for(size_t iSample = 0; iSample < nSamples; ++iSample)
        {
            factor = 1.0 -  (1.0 - factor) * decayFactor;

            peak[0] *= decayFactor;
            peak[0] = std::max(std::abs(input[iSample]), peak[0]);

            double max = std::abs(input[iSample]);

            if(max > Threshold)
            {
                double inverse = std::pow(0.5, (max - Threshold) / Knee);
                double limited = Threshold + Knee * (1 - inverse);

                factor = std::min(limited / max, factor);
            }

            input[iSample] *= factor;
        }
    }

    void run(std::vector<Signal>& input)
    {
        assert(peak.size() == input.size());

        size_t nChannels = input.size();
        size_t nSamples = input[0].size();

        for(size_t iSample = 0; iSample < nSamples; ++iSample)
        {
            factor = 1.0 -  (1.0 - factor) * decayFactor;

            for(size_t iChannel = 0; iChannel < nChannels; ++ iChannel)
            {
                peak[iChannel] *= decayFactor;
                peak[iChannel] = std::max(std::abs(input[iChannel][iSample]), peak[iChannel]);
            }

            double max = 0;
            for(const auto& channel : input)
            {
                max = std::max(std::abs(channel[iSample]), max);
            }

            if(max > Threshold)
            {
                double inverse = std::pow(0.5, (max - Threshold) / Knee);
                double limited = Threshold + Knee * (1 - inverse);

                factor = std::min(limited / max, factor);
            }

            for(auto& channel : input)
            {
                channel[iSample] *= factor;
            }
        }
    }

    double getFactor() const { return factor; }
    std::vector<double> getPeak() const { return peak; }

private:
    static constexpr double Threshold = 0.9;
    static constexpr double Knee = 1 - Threshold;

    double factor = 1.0;
    double decayFactor;
    std::vector<double> peak;
};
