#pragma once

#include "ImpulseResponse.hpp"
#include "RealFFT/RealFFT.hpp"
#include "Utils/LogTaper.hpp"

class LogSpectrum
{
    using Signal = std::vector<double>;
    using Spectrum = std::vector<std::complex<double>>;

public:
    struct Point
    {
        Point(double frequency, double amplitude)
            : frequency(frequency), amplitude(amplitude)
        {}

        double frequency;
        double amplitude;
    };

    LogSpectrum() = default;

    LogSpectrum(const ImpulseResponse& impulseResponse, unsigned sampleRate)
    {
        size_t nChannels = impulseResponse.nChannels();
        size_t nSamples = impulseResponse.nSamples();

        size_t fftLength = 2 * Length;
        size_t decimation = 1;

        while(fftLength < nSamples)
        {
            fftLength *= 2;
            decimation *= 2;
        }

        RealFFT plotFFT(fftLength);

        for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
        {
            Signal signal = impulseResponse.getChannel(iChannel);
            signal.resize(fftLength, 0);

            Spectrum spectrum(fftLength / 2 + 1);
            plotFFT.forward(signal, spectrum);

            std::vector<Point> channelSpectrum;

            for(size_t iBin = 0; iBin < Length; ++iBin)
            {
                double value = 0;

                for(size_t iSubBin = 0; iSubBin < decimation; ++iSubBin)
                {
                    size_t iSample = iBin * decimation + iSubBin;
                    value += std::abs(spectrum.at(iSample)) / decimation;
                }

                double frequency = static_cast<double>(iBin + 1) / Length * sampleRate / 2;

                double logValue = LogTaper::toDB(value);

                if(std::isfinite(logValue))
                {
                    channelSpectrum.push_back(Point(frequency, logValue));
                }
                else
                {
                    channelSpectrum.push_back(Point(frequency, std::numeric_limits<double>::lowest()));
                }
            }

            data.push_back(channelSpectrum);
        }
    }

    const std::vector<std::vector<Point>>& get() const
    {
        return data;
    }

    bool empty() const
    {
        return data.empty();
    }

    static constexpr size_t Length{512};

private:
    std::vector<std::vector<Point>> data;
};
