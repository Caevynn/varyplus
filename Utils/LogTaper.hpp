#pragma once

#include <cmath>

class LogTaper
{
public:
    static double fromLog(double logValue, double linearRange = 1.0)
    {
        double normalizedLinearValue = a * std::exp(b * logValue) - a;
        double linearValue = normalizedLinearValue * linearRange;
        return linearValue;
    }

    static double toLog(double linearValue, double linearRange = 1.0)
    {
        double normalizedLinearValue = linearValue / linearRange;
        double logValue = std::log((normalizedLinearValue + a) / a) / b;
        return logValue;
    }

    static double toDB(double linearValue)
    {
        double dBValue = 20 * std::log10(linearValue);
        return dBValue;
    }

    static double fromDB(double dBValue)
    {
        double linearValue = std::pow(10, dBValue / 20);
        return linearValue;
    }

private:
    static constexpr double a{0.031623};
    static constexpr double b{3.485};

    // a = 1 / 10^(dbRange / 20)
    // b = std::log(1 / a + 1);
    // Pairs would be:
    // - [0.0316, 3.485] @ 30 dB
    // - [0.001, 6.9088] @ 60 dB
};
