#include "OverlapAdd.hpp"

OverlapAdd::OverlapAdd(size_t blockSize, size_t nChannels) :
    blockSize(blockSize),
    nChannels(nChannels),
    previousSignals(nChannels, Signal(blockSize, 0)),
    outputBuffers(nChannels, Signal(4 * blockSize, 0)),
    windowedSignal(2 * blockSize, 0),
    filteredSignal(4 * blockSize, 0),
    window(2 * blockSize),
    fft(4 * blockSize)
{}

void OverlapAdd::reset()
{
    for(auto& signal : previousSignals)
    {
        std::fill(signal.begin(), signal.end(), 0);
    }

    for(auto& signal : outputBuffers)
    {
        std::fill(signal.begin(), signal.end(), 0);
    }
}

void OverlapAdd::input(const Signal& inputSignal, Spectrum& inputSpectrum)
{
    auto& previousSignal = previousSignals[0];

    assert(inputSignal.size() == blockSize);
    assert(previousSignal.size() == blockSize);
    assert(inputSpectrum.size() == 2 * blockSize + 1);

    // Combine with last block and apply window function
    std::copy(previousSignal.begin(), previousSignal.end(), windowedSignal.begin());
    std::copy(inputSignal.begin(), inputSignal.end(), windowedSignal.begin() + blockSize);
    window.apply(windowedSignal);

    // Store input signal
    std::copy(inputSignal.begin(), inputSignal.end(), previousSignal.begin());

    // Apply FFT
    fft.forwardWithZeropadding(windowedSignal, inputSpectrum);
}

void OverlapAdd::input(const std::vector<Signal>& inputSignals, std::vector<OverlapAdd::Spectrum>& inputSpectrums)
{
    assert(inputSignals.size() == nChannels);
    assert(inputSpectrums.size() == nChannels);
    assert(previousSignals.size() == nChannels);

    for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
    {
        const auto& inputSignal = inputSignals[iChannel];
        auto& inputSpectrum = inputSpectrums[iChannel];
        auto& previousSignal = previousSignals[iChannel];

        assert(inputSignal.size() == blockSize);
        assert(inputSpectrum.size() == 2 * blockSize + 1);
        assert(previousSignal.size() == blockSize);

        // Combine with last block and apply window function
        std::copy(previousSignal.begin(), previousSignal.end(), windowedSignal.begin());
        std::copy(inputSignal.begin(), inputSignal.end(), windowedSignal.begin() + blockSize);
        window.apply(windowedSignal);

        // Store input signal
        std::copy(inputSignal.begin(), inputSignal.end(), previousSignal.begin());

        // Apply FFT
        fft.forwardWithZeropadding(windowedSignal, inputSpectrum);
    }
}

void OverlapAdd::output(std::vector<Spectrum>& outputSpectrums, std::vector<OverlapAdd::Signal>& outputSignals)
{
    assert(outputSpectrums.size() == nChannels);
    assert(outputBuffers.size() == nChannels);
    assert(outputSignals.size() == nChannels);

    for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
    {
        auto& outputSpectrum = outputSpectrums[iChannel];
        auto& outputBuffer = outputBuffers[iChannel];
        auto& outputSignal = outputSignals[iChannel];

        assert(outputSpectrum.size() == 2 * blockSize + 1);
        assert(outputBuffer.size() == 4 * blockSize);
        assert(outputSignal.size() == blockSize);

        // Apply IFFT
        fft.inverse(outputSpectrum, filteredSignal);

        // Overlap and add
        std::transform(filteredSignal.begin(), filteredSignal.end() - blockSize, outputBuffer.begin() + blockSize, outputBuffer.begin(), std::plus<double>());
        std::copy(filteredSignal.begin() + 3 * blockSize, filteredSignal.end(), outputBuffer.begin() + 3 * blockSize);

        // Copy to output
        std::copy_n(outputBuffer.begin(), blockSize, outputSignal.begin());
    }
}
