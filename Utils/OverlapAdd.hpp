#pragma once

#include "HannWindow.hpp"
#include "RealFFT/RealFFT.hpp"

class OverlapAdd
{
    using Signal = std::vector<double>;
    using Spectrum = std::vector<std::complex<double>>;

public:
    OverlapAdd(size_t blockSize = 0, size_t nChannels = 0);

    void reset();
    void input(const Signal& inputSignal, Spectrum& inputSpectrum);
    void input(const std::vector<Signal>& inputSignals, std::vector<Spectrum>& inputSpectrums);
    void output(std::vector<Spectrum>& outputSpectrums, std::vector<Signal>& outputSignals);

private:
    size_t blockSize, nChannels;

    std::vector<Signal> previousSignals;
    std::vector<Signal> outputBuffers;
    Signal windowedSignal;
    Signal filteredSignal;

    HannWindow window;
    RealFFT fft;
};
