#pragma once

#include <vector>
#include <complex>
#include <cassert>

class OverlapAddBuffer
{
    using Spectrum = std::vector<std::complex<double>>;

public:
    class Channel
    {
    public:
        struct Iterator
        {
            using iterator_category = std::forward_iterator_tag;
            using difference_type   = std::ptrdiff_t;
            using value_type        = Spectrum;
            using pointer           = value_type*;
            using reference         = value_type&;

            Iterator(Channel* channel, size_t position) : channel(channel), position(position) {}

            reference operator*() { return channel->at(position); }
            pointer operator->() { return &channel->at(position); }
            Iterator& operator++() { position ++; return *this; }
            Iterator operator++(int) { Iterator tmp = *this; ++(*this); return tmp; }
            friend bool operator== (const Iterator& a, const Iterator& b) { return a.position == b.position; };
            friend bool operator!= (const Iterator& a, const Iterator& b) { return a.position != b.position; };

        private:
            Channel* channel;
            size_t position;
        };

        struct ConstIterator
        {
            using iterator_category = std::forward_iterator_tag;
            using difference_type   = std::ptrdiff_t;
            using value_type        = const Spectrum;
            using pointer           = value_type*;
            using reference         = value_type&;

            ConstIterator(const Channel* channel, size_t position) : channel(channel), position(position) {}

            reference operator*() const { return channel->at(position); }
            pointer operator->() const { return &channel->at(position); }
            ConstIterator& operator++() { position ++; return *this; }
            ConstIterator operator++(int) { ConstIterator tmp = *this; ++(*this); return tmp; }
            friend bool operator== (const ConstIterator& a, const ConstIterator& b) { return a.position == b.position; };
            friend bool operator!= (const ConstIterator& a, const ConstIterator& b) { return a.position != b.position; };

        private:
            const Channel* channel;
            size_t position;
        };

        Iterator begin() { return Iterator(this, 0); }
        Iterator end() { return Iterator(this, size()); }
        ConstIterator begin() const { return ConstIterator(this, 0); }
        ConstIterator end() const { return ConstIterator(this, size()); }
        ConstIterator cbegin() const { return ConstIterator(this, 0); }
        ConstIterator cend() const { return ConstIterator(this, size()); }

        Channel(size_t nEntries, size_t nSamples)
            : nEntries(nEntries),
              spectrums(nEntries, Spectrum(nSamples, 0))
        {}

        void push_back(const Spectrum& spectrum)
        {
            assert(spectrum.size() == spectrums[currentIndex].size());
            currentIndex = (currentIndex + 1) % nEntries;
            std::copy(spectrum.begin(), spectrum.end(), spectrums[currentIndex].begin());
        }

        const Spectrum& at(size_t position) const
        {
            assert(2 * position < nEntries);
            size_t iEntry = (currentIndex + nEntries - 2 * position) % nEntries;
            return spectrums[iEntry];
        }

        Spectrum& at(size_t position)
        {
            assert(2 * position < nEntries);
            size_t iEntry = (currentIndex + nEntries - 2 * position) % nEntries;
            return spectrums[iEntry];
        }

        size_t size() const { return spectrums.size() / 2; }

    private:
        size_t nEntries;
        size_t currentIndex = 0;
        std::vector<Spectrum> spectrums;
    };

    std::vector<Channel>::iterator begin() { return buffer.begin(); }
    std::vector<Channel>::iterator end() { return buffer.end(); }
    std::vector<Channel>::const_iterator begin() const { return buffer.begin(); }
    std::vector<Channel>::const_iterator end() const { return buffer.end(); }
    std::vector<Channel>::const_iterator cbegin() const { return buffer.cbegin(); }
    std::vector<Channel>::const_iterator cend() const { return buffer.cend(); }

    OverlapAddBuffer(size_t dataSize, size_t bufferSize, size_t nChannels)
        : nChannels(nChannels), nEntries(2 * bufferSize), nSamples(dataSize)
    {
        assert(nChannels != 0 && nEntries != 0 && nSamples != 0);
        reset();
    }

    void push_back(const Spectrum& spectrum)
    {
        for(auto& channel : buffer)
        {
            channel.push_back(spectrum);
        }
    }

    void push_back(const std::vector<Spectrum>& spectrums)
    {
        assert(spectrums.size() == nChannels);

        auto spectrumIt = spectrums.begin();
        for(auto& channel : buffer)
        {
            channel.push_back(*spectrumIt++);
        }
    }

    const Spectrum& at(size_t position, size_t iChannel) const
    {
        assert(iChannel < nChannels);
        return buffer[iChannel].at(position);
    }

    size_t size() const { return buffer.size(); }

    void reset()
    {
        buffer.clear();
        buffer.resize(nChannels, Channel(nEntries, nSamples));
    }

private:
    size_t nChannels, nEntries, nSamples;

    std::vector<Channel> buffer;
};
