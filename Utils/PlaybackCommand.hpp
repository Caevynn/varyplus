#pragma once

#include <variant>

// Helpers for visiting std::variant
template<class> inline constexpr bool always_false_v = false;
template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

struct VolumeCommand
{
    VolumeCommand(double value) : value(value) {}
    double value;
};

struct PositionCommand
{
    PositionCommand(size_t value) : value(value) {}
    size_t value;
};

struct OutputCommand
{
    OutputCommand(size_t value) : value(value) {}
    size_t value;
};

struct PlayCommand
{
    PlayCommand(bool value) : value(value) {}
    bool value;
};

using PlaybackCommandVariant = std::variant<std::monostate, VolumeCommand, PositionCommand, OutputCommand, PlayCommand>;

struct PlaybackCommand
{
    PlaybackCommand() = default;

    PlaybackCommand(size_t iChannel, PlaybackCommandVariant command)
        : iChannel(iChannel), variant(command)
    {}

    size_t iChannel;
    PlaybackCommandVariant variant;
};

/*class PlaybackCommand
{
public:
    enum Type
    {
        Start,
        Stop,
        Volume,
        Output,
        Position,
        Invalid
    };

    PlaybackCommand()
        : type(Type::Invalid)
    {}

    static PlaybackCommand start(size_t channel) { return PlaybackCommand(Type::Start, channel, 0, 0); }
    static PlaybackCommand stop(size_t channel) { return PlaybackCommand(Type::Stop, channel, 0, 0); }
    static PlaybackCommand volume(size_t channel, double volume) { return PlaybackCommand(Type::Volume, channel, 0, volume); }
    static PlaybackCommand output(size_t channel, size_t output) { return PlaybackCommand(Type::Output, channel, output, 0); }
    static PlaybackCommand position(size_t channel, size_t position) { return PlaybackCommand(Type::Position, channel, position, 0); }

    bool isValid() const { return type != Type::Invalid; }
    Type getType() const { return type; }
    size_t getChannel() const { return channel; }

    double getVolume() const
    {
        assert(type == Type::Volume);
        return doubleValue;
    }

    size_t getOutput() const
    {
        assert(type == Type::Output);
        return sizeValue;
    }

    size_t getPosition() const
    {
        assert(type == Type::Position);
        return sizeValue;
    }

private:
    PlaybackCommand(Type type, size_t channel, size_t sizeValue, double doubleValue)
        : type(type),
          channel(channel),
          sizeValue(sizeValue),
          doubleValue(doubleValue)
    {}

    Type type;
    size_t channel;
    size_t sizeValue;
    double doubleValue;
};*/
