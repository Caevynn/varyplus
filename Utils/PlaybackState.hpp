#pragma once

#include "PlaybackCommand.hpp"

class PlaybackPosition
{
public:
    PlaybackPosition(size_t length = 0) : position(0), length(length) {}

    void setPosition(size_t position) { this->position = position; }
    void setRelativePosition(double position) { this->position = length * position; }
    void increasePosition(size_t nSamples) { position += nSamples; }

    size_t getPosition() const { return position; }
    size_t getLength() const { return length; }
    double getRelativePosition() const { return static_cast<double>(position) / length; }

private:
    size_t position;
    size_t length;
};

class PlaybackState
{
public:
    PlaybackState(size_t length)
        : position(length)
    {}

    void increasePositionIfRunning(size_t blockSize)
    {
        if(running && position.getLength() > 0)
        {
            position.increasePosition(blockSize);

            if(position.getPosition() >= position.getLength())
            {
                position.setPosition(0);
                running = false;
            }
        }
    }

    void apply(const PlaybackCommand& command)
    {
        std::visit(overloaded{
            [](auto) {},
            [this](VolumeCommand command) { volume = command.value; },
            [this](PositionCommand command) { position.setPosition(command.value); },
            [this](OutputCommand command) { output = command.value; },
            [this](PlayCommand command) { running = command.value; },
        }, command.variant);
    }

    bool isRunning() const { return running; }
    double getVolume() const { return volume; }
    size_t getOutput() const { return output; }
    PlaybackPosition getPosition() const { return position; }

private:
    bool running{false};
    double volume{1.0};
    size_t output{0};
    PlaybackPosition position;
};
