#pragma once

#include <vector>
#include <cmath>

class Sinc
{
    using Signal = std::vector<double>;

public:
    static Signal create(unsigned sampleRate, double frequency, size_t length)
    {
        size_t nSamples = static_cast<double>(length * sampleRate) / frequency;

        Signal sinc;
        sinc.reserve(nSamples);

        for(size_t iSample = 0; iSample < nSamples; ++iSample)
        {
            double position = static_cast<double>(iSample) - nSamples / 2;
            double time = position / sampleRate;

            if(position == 0)
            {
                sinc.push_back(1.0);
            }
            else
            {
                double value = 2 * M_PI * frequency * time;
                sinc.push_back(std::sin(value) / value);
            }
        }

        return sinc;
    }
};
