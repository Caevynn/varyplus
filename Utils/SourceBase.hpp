#pragma once

#include <vector>
#include <string>
#include <optional>

class SourceBase
{
    using Signal = std::vector<double>;

public:
    SourceBase(const std::string& name) : name(name) {}
    virtual ~SourceBase() = default;

    virtual void reset() = 0;
    virtual void read(size_t nSamples, Signal& signal) = 0;

    virtual void read(size_t /*position*/, size_t nSamples, Signal& signal) { read(nSamples, signal); }
    virtual void setPosition(size_t /*position*/) {}
    virtual size_t getPosition() const { return 0; }
    virtual size_t getLength() const { return 0; }

    const std::string& getName() const { return name; }

private:
    std::string name;
};
