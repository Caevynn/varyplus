#include "TransferFunction.hpp"

#include <algorithm>
#include <cassert>

#include "RealFFT/RealFFT.hpp"

#include "ImpulseResponse.hpp"
#include "OverlapAddBuffer.hpp"

TransferFunction::TransferFunction(const ImpulseResponse& impulseResponse, size_t blockSize, unsigned sampleRate, const std::string& name)
    : TransferFunctionBase(blockSize, name),
      logSpectrum(impulseResponse, sampleRate)
{
    size_t nChannels = impulseResponse.nChannels();
    size_t nSamples = impulseResponse.nSamples();
    size_t partitionSize = 2 * blockSize;
    size_t nPartitions = nSamples / partitionSize + 1;

    RealFFT fft(2 * partitionSize);

    for(size_t iChannel = 0; iChannel < nChannels; ++iChannel)
    {
        const std::vector<double>& channel = impulseResponse.getChannel(iChannel);

        auto channelIt = channel.begin();

        std::vector<Spectrum> partitionList(nPartitions, Spectrum(partitionSize + 1));
        size_t position = 0;

        for(size_t iPartition = 0; iPartition < nPartitions; ++iPartition)
        {
            size_t remainingSize = nSamples - position;

            Signal partition;
            size_t copyCnt = std::min(remainingSize, partitionSize);
            std::copy_n(channelIt, copyCnt, std::back_inserter(partition));
            partition.resize(partitionSize, 0);

            fft.forwardWithZeropadding(partition, partitionList[iPartition]);

            position += partitionSize;
            channelIt += partitionSize;
        }

        channels.push_back(partitionList);
    }
}

void TransferFunction::apply(const OverlapAddBuffer &input, std::vector<Spectrum>& output) const
{
    assert(input.size() == getNumChannels());
    assert(output.size() == getNumChannels());
    assert(channels.size() == getNumChannels());

    auto inputChannelIt = input.cbegin();
    auto filterChannelIt = channels.cbegin();
    for(auto& outputChannel : output)
    {
        assert(outputChannel.size() == getSpectrumSize());

        std::fill(outputChannel.begin(), outputChannel.end(), 0);

        assert(inputChannelIt->size() == getNumPartitions());
        assert(filterChannelIt->size() == getNumPartitions());

        auto inputPartitionIt = inputChannelIt->cbegin();
        for(const auto& filterPartition : *filterChannelIt)
        {
            assert(inputPartitionIt->size() == getSpectrumSize());
            assert(filterPartition.size() == getSpectrumSize());

            auto inputSampleIt = inputPartitionIt->cbegin();
            auto filterSampleIt = filterPartition.cbegin();
            for(auto& sample : outputChannel)
            {
                sample += *inputSampleIt++ * *filterSampleIt++;
            }

            ++inputPartitionIt;
        }

        ++inputChannelIt;
        ++filterChannelIt;
    }
}
