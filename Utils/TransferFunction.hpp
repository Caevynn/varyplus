#pragma once

#include <vector>
#include <complex>

#include "TransferFunctionBase.hpp"
#include "Utils/LogSpectrum.hpp"

class ImpulseResponse;
class OverlapAddBuffer;

class TransferFunction : public TransferFunctionBase
{
    using Signal = std::vector<double>;
    using Spectrum = std::vector<std::complex<double>>;

public:
    TransferFunction(const ImpulseResponse& impulseResponse, size_t blockSize, unsigned sampleRate, const std::string& name);

    void apply(const OverlapAddBuffer& input, std::vector<Spectrum>& output) const override;

    size_t getNumChannels() const override { return channels.size(); }
    size_t getNumPartitions() const override { return channels.at(0).size(); }
    size_t getSpectrumSize() const override { return channels.at(0).at(0).size(); }

    const LogSpectrum& getLogSpectrum() const override { return logSpectrum; }

private:
    std::vector<std::vector<Spectrum>> channels;

    LogSpectrum logSpectrum;
};
