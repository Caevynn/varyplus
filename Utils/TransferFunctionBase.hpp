#pragma once

#include <vector>
#include <complex>
#include <string>

class OverlapAddBuffer;
class LogSpectrum;

class TransferFunctionBase
{
    using Spectrum = std::vector<std::complex<double>>;

public:
    virtual ~TransferFunctionBase() = default;

    virtual void apply(const OverlapAddBuffer& input, std::vector<Spectrum>& output) const = 0;

    virtual size_t getNumPartitions() const = 0;
    virtual size_t getSpectrumSize() const = 0;
    virtual size_t getNumChannels() const = 0;

    virtual const LogSpectrum& getLogSpectrum() const = 0;

    size_t getBlockSize() const { return blockSize; }
    const std::string& getName() const { return name; }

protected:
    TransferFunctionBase(size_t blockSize, const std::string& name)
        : blockSize(blockSize),
          name(name)
    {}

private:
    size_t blockSize;
    std::string name;
};
