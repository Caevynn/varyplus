#pragma once

#include <vector>
#include <optional>

class VolumeControl
{
    using Signal = std::vector<double>;

public:
    void run(Signal& input)
    {
        if(!newAmplitude)
        {
            for(auto& sample : input)
            {
                sample *= amplitude;
            }
        }
        else
        {
            size_t nSamples = input.size();

            for(size_t iSample = 0; iSample < nSamples; ++iSample)
            {
                double relativePosition = static_cast<double>(iSample) / nSamples;

                double currentAmplitude = relativePosition * *newAmplitude + (1 - relativePosition) * amplitude;

                input[iSample] *= currentAmplitude;
            }

            amplitude = *newAmplitude;
            newAmplitude = std::nullopt;
        }
    }

    void run(std::vector<Signal>& input)
    {
        if(!newAmplitude)
        {
            for(auto& channel : input)
            {
                for(auto& sample : channel)
                {
                    sample *= amplitude;
                }
            }
        }
        else
        {
            size_t nChannels = input.size();
            size_t nSamples = input[0].size();

            for(size_t iSample = 0; iSample < nSamples; ++iSample)
            {
                double relativePosition = static_cast<double>(iSample) / nSamples;
                double currentAmplitude = relativePosition * *newAmplitude + (1 - relativePosition) * amplitude;

                for(size_t iChannel = 0; iChannel < nChannels; ++ iChannel)
                {
                    input[iChannel][iSample] *= currentAmplitude;
                }
            }

            amplitude = *newAmplitude;
            newAmplitude = std::nullopt;
        }
    }

    void setAmplitude(double amplitude) { newAmplitude = amplitude; }
    double getAmplitude() const { return amplitude; }

private:
    double amplitude{1.0};
    std::optional<double> newAmplitude;
};
